/*
 Navicat Premium Data Transfer

 Source Server         : mysql local
 Source Server Type    : MySQL
 Source Server Version : 100417
 Source Host           : localhost:3306
 Source Schema         : venta_migracion

 Target Server Type    : MySQL
 Target Server Version : 100417
 File Encoding         : 65001

 Date: 13/07/2021 22:34:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cliente
-- ----------------------------
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente`  (
  `cliente_id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cliente_documento` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tipo_cliente_id` int(11) NULL DEFAULT NULL,
  `cliente_direccion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cliente_telefono` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cliente_correo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cliente_estado` int(255) NULL DEFAULT 1,
  `cliente_fecha_nacimiento` date NULL DEFAULT NULL,
  `cliente_distrito` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cliente_provincia` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cliente_departamento` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cliente_fecha_registro` date NULL DEFAULT NULL,
  `cliente_tipo_comprobabte` int(255) NULL DEFAULT 1 COMMENT '1 boleta;2 factura',
  `cliente_ruc` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cliente_razon_social` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cliente_direccion_empresa` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cliente_id`) USING BTREE,
  INDEX `tipo_cliente_id`(`tipo_cliente_id`) USING BTREE,
  CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`tipo_cliente_id`) REFERENCES `tipo_cliente` (`tipo_cliente_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 190 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cliente
-- ----------------------------
INSERT INTO `cliente` VALUES (1, 'Cliente Varios', '0000001', 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (3, 'dominguez lecca soila carina', '73381324', 1, 'nueva esperanza nueva esperanza', '', 'soila.carina26@gmail.com', 1, '1900-01-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (4, 'Robles Ruiz Adaluz', '71908165', 1, 'TOCACHE', '', 'roblesruizadaluz@gmail.com', 1, '1995-01-17', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (5, 'HERNANDEZ CAMPOS MARIA', '45010411', 1, 'Jr Pegro Gomez cdra 01', '', 'mariacampos110985@gmail.com', 1, '1985-09-11', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (6, 'hilario rios katia celmira', '46361605', 1, 'jr tocache cuadra 5', '', 'actualizatucorreo@sokso.com', 1, '1990-05-10', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (7, 'ISIDRO ESPINOZA YESICA MARY', '47258269', 1, 'PROGRESO', '', 'actualizatucorreo@sokso.com', 1, '1992-08-24', 'NUEVO PROGRESO', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (8, 'SANTISTEBAN DOMINGUEZ YENI SABINA', '71902263', 1, 'JR: GERMAN RENGIFO C-09', '', 'yeni.sd.1999@gmail.com', 1, '1980-01-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (9, 'VASQUEZ ROJAS SUSAN', '71741199', 1, 'JR. ESTEBAN DELGADO CDRA 18 TOCACHE', '', 'SUSAN18@HOTMAIL.COM', 1, '1992-10-26', 'HUANUCO', 'HUANUCO', 'HUANUCO', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (10, 'Mozombite beltran Sayuri', '71702364', 1, 'Jr modesto perez # 815', '', 'Sayurimozombite998@gmail.com', 1, '1900-01-01', 'NUEVO PROGRESO', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (11, 'siche reategui karen veronica', '47569325', 1, 'Jr. pedro Gomez con Sara Anita', '', 'karen_loveyou_91@hotmail.com', 1, '1998-09-03', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (12, 'Olivera Novoa Genny Lallani', '77490118', 1, 'lavadero olivera nuevo progreso', '', 'Jessynu15@gmail.com', 1, '1999-03-21', 'NUEVO PROGRESO', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (13, 'TAFUR MIRANDA JANETH ELIANA', '61744968', 1, 'JR JORGE CHAVES CDRA 10', '', 'actualizatucorreo@sokso.com', 1, '1995-12-15', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (14, 'VASQUEZ RUIZ KELLY', '42711442', 1, 'NUEVO PROGRESO', '', 'actualizatucorreo@sokso.com', 1, '1984-09-02', 'NUEVO PROGRESO', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (15, 'RAMIREZ VELASQUEZ AYMETH', '70262897', 1, 'TOCACHE', '', 'aymethramirezvelasquez@gmail.com', 1, '1999-07-19', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (16, 'morales ruiz milagros del pilar', '46801214', 1, 'av aviacion 800', '', 'milagrosmoralesruiz@gmail.com', 1, '1989-10-10', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (17, 'PEÑA VILLANUEVA SONIA ELVA', '44549892', 1, 'JR. PEDRO GOMEZ CDRA 2 JR. PEDRO GO  MEZ CDRA 2', '', 'actualizatucorreo@sokso.com', 1, '2018-06-06', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (18, 'FELIPE VASQUEZ JOISY OFELIA', '44220471', 1, 'JR. TOCACHE 110', '', 'JOISYFELIPE34@HOTMAIL.COM', 1, '1987-03-25', 'HUANUCO', 'HUANUCO', 'HUANUCO', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (19, 'FIGUEROA ECHEVARRIA JHOSELIN AMANDA', '76077876', 1, 'CUARTO SECTOR LIMON LAS PALMERAS', '', 'jhoselinfigueroa.9610@gmail.com', 1, '1996-07-10', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (20, 'HORNA RUIZ ANGELA ELIZABET', '73476951', 1, 'invacion almendras', '', 'hr_angela_97@hotmail.com', 1, '1997-03-14', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (21, 'PEREZ AREVALO KIARA ESTHER', '71991551', 1, 'JR FREDDY ALIAGA 140', '', 'kiara.perezarevalo@gmail.com', 1, '1981-11-12', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (22, 'MEZA MEDINA CARMEN', '47259960', 1, 'Jorge chavez c/2 sin numero', '', 'carmencitalove_kiss@hotmail.com', 1, '1991-07-31', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (23, 'MESA VILLAFUERTE TOLY', '47579184', 1, 'TOCACHE', '', 'Mariajulialuis123@gmail.com', 1, '1988-08-22', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (24, 'sanchez  perez Crisler', '01040552', 1, 'los jardines', '', 'actualizatucorreo@sokso.com', 1, '1971-07-23', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (25, 'GUTIERREZ RENGIFO SANDY FRANIXHA', '45587195', 1, 'Fredy aliaga cdra 12', '', 'sandyfranixhag@gmail.com', 1, '1988-07-02', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (26, 'FLORES VELASQUEZ THALIA', '76376180', 1, 'TOCACHE', '', 'floresvelasqueztalia@gmail.com', 1, '1996-10-13', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (27, 'Rengifo Romero Dolly', '16806412', 1, 'Jr. Jorge Chávez número 1053', '', 'dollyrengifo@hotmail.com', 1, '1978-09-20', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (28, 'RENGIFO PANDURO LIZ AMPARO', '43417405', 1, 'AV tupac Amaru CDRA 03', '', 'larp1405@gmail.com', 1, '1986-01-27', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (29, 'Vasquez del Castillo Jobani', '41733632', 1, 'TOCACHE', '', 'delva_14@hotmail.com', 1, '1983-02-28', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (30, 'ACUÑA HURTADO VICTOR', '41679890', 1, 'Jr Jorgue Chavez cdra 15', '', 'victoracuna91@gmail.com', 1, '1982-01-18', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (31, 'VARGAS AGUILAR NATHALY PAOLA', '44575637', 1, 'AV BELAUNDE 1060', '', 'nathycita86@gmaill.com', 1, '1986-10-27', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (32, 'DEL AGUILA MOSQUERA MERCEDES', '46564724', 1, 'Jirón Esteban Delgado cuadra 2', '', 'mdelaguilamosquera@gmail.com', 1, '1968-06-18', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (33, 'AGUILAR MONTOYA CLAUDIA RENEE', '46940811', 1, 'Jr. Progreso N 465 Int. D-6', '', 'clauaguimon@gmail.com', 1, '1992-01-20', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (34, 'Puyo Ordoñez luciola', '44959112', 1, 'JR Bolognesi Crda 15', '', 'actualizatucorreo@sokso.com', 1, '1977-07-25', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (35, 'MONTEJO RIOS ZULLY YASIRA', '61968958', 1, 'Jr Leonando Hidalgo Crda 02', '', 'actualizatucorreo@sokso.com', 1, '2002-05-14', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-03-07', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (36, 'ZURITA ARANDA LUZ MARIA', '76402151', 1, 'JR los conquistadores cdra 02', '', 'luzmariazuritaaranda@gmail.com', 1, '1998-12-04', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-05-06', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (37, 'SAAVEDRA FLORES KAREN GIANINA', '74765153', 1, 'JR. José Olaya 514', '', 'karen_2006_20@hotmail.com', 1, '1994-11-20', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-05-21', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (38, 'Mozombite Luna Janny Anabel', '42492280', 1, 'Jr fredy Aliaga 372', '', 'jannym84@hotmail.com', 1, '1984-06-06', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (39, 'Alcahuaman Matos Janet Lidia', '40497310', 1, 'carettera fernando belaunde terrry', '', 'actualizatucorreo@sokso.com', 1, '1980-02-06', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (40, 'LOPEZ RENGIFO LADY KELLY', '45255051', 1, 'JR ALFONSO UGARTE 395', '', 'lake_lorengifo@hotmail.com', 1, '1988-07-22', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (41, 'ALVAREZ BARZOLA JUDITH KEIKO', '72178747', 1, 'JIRON BOLOGNESI 908', '', 'keiko02.04.95@gmail.com', 1, '1995-04-02', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (42, 'Ramos Margarin sarai', '73581022', 1, 'jr.Daudilio Zavaleta', '', 'juana14_14@hotmail.com', 1, '1994-02-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (43, 'Flores Hernandez Mirtha Luz', '40821016', 1, 'Jr fredy aliaga 44', '', 'mirtha_jh@hotmail.com', 1, '1981-02-28', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (44, 'Hernandez Campos Erlinda', '47554312', 1, 'Jr pedro Gomes Cdra 01', '', 'Dylanmarquinah@gmail.com', 1, '1993-02-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (45, 'OCTAVILA CHAVEZ FONSECA', '00998706', 1, 'Av Aviacion 507 Av Aviacion 507', '', 'vilachavez@hotmail.com', 1, '1954-01-10', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (46, 'DOMINGUEZ NAJAR RUDDY MIRENA', '45969304', 1, 'Jr. Bolognesi c-03', '', 'soniaelvapevi@gmail.com', 1, '1989-10-04', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (47, 'GARCIA CABRERA JEANETTE IVONNE', '33818624', 1, 'Jr MIGUEL GRAU 256', '', 'garcab5@hotmail.com', 1, '1974-12-05', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (48, 'arevalo rodriguez gladis', '01013477', 1, 'polvora', '', 'gladys_lali@hotmail.com', 1, '1971-07-27', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (49, 'CATILLO TINOCO IRIS LYSSEÑA', '71975414', 1, 'JR. PROLONGACIÓN JORGE CHAVEZ CDRA. 01', '', 'castillotinocoi@gmail.com', 1, '1995-10-14', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-02-11', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (50, 'MENDIETA CAMPOS MIRIAN KELY', '48319791', 1, 'JR MIGUEL GRAU CDRA 03', '', 'Miriancampos94@hotmail.com', 1, '1994-03-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-03-31', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (51, 'MENDOZA ATERO NORA', '44880406', 1, 'Av. España s/n _ pampayacu', '', 'noramendozaatero23@gmail.com', 1, '1988-01-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-05-28', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (52, 'Ranos herrada Evelin', '62167894', 1, 'URB. CERCADO JR. PEGRO GOMEZ S/N', '', 'Evelinramos13tv@gmail.com', 1, '1990-01-09', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (53, 'Villanueva Dominguez Efrocinia', '00961106', 1, 'Jr.pedro gomez esq.jr.grimaldo reategui', '', 'actualizatucorreo@sokso.com', 1, '1900-01-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (54, 'Gui Tuanama Mayra', '43154470', 1, 'Av. Perú 542', '', 'Mayra_rt20@hotmail.com', 1, '1980-01-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (55, 'CARHUAPOMA HUERTA IRIS BEATRIZ', '47564744', 1, 'Jr Grimaldo Reategui cdra 03', '', 'angeluz270287@gmail.com', 1, '1991-07-22', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (56, 'AGUIRRE MALQUI EMILIA', '33819205', 1, 'JR FREDI ALIGA', '', 'actualizatucorreo@sokso.com', 1, '1975-07-29', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (57, 'murrieta cachique kendra', '76661679', 1, 'Jr chorro san juan cdra 02', '', 'kmurrietacachique@gmail.com', 1, '1995-12-24', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (58, 'MESTANZA QUISPE MARIELA', '42067278', 1, 'TOCACHE', '', 'actualizatucorreo@sokso.com', 1, '1983-05-27', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (59, 'Del Aguila Rojas Karin Sheyla', '43969035', 1, 'Jr. Jorge Chavez N°1201 Jr. Jorge Chavez N°1201', '', 'karinsheyla@gmail.com', 1, '1986-05-21', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (60, 'HUAMAN ALANYA ANNA FLORISTELLA', '47783514', 1, 'TOCACHE', '', 'annahuamanalanya29@gmail.com', 1, '1991-06-23', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (61, 'NAVARRO ALVAREZ MONICA ELISA', '42892350', 1, 'JR DAUDILIO ZAVALETA 226', '', 'menavarroalvarez@gmail.com', 1, '1984-12-11', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (62, 'ORTIZ SHUPINGAHUA MARIA', '41882979', 1, 'Jr Grimaldo Reategui cdra 02', '', 'mortizshu1@gmail.com', 1, '1983-01-04', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (63, 'DOMINGUEZ CARRERA YULAN MILAN', '47259932', 1, 'JR. SAN MARTIN C/15', '', 'actualizatucorreo@sokso.com', 1, '1992-08-03', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (64, 'GONZALES DEL AGUILA ROSA FIDELIA', '44067922', 1, 'JR. FEDERICO VILLARREAL C-2', '', 'FIDELIA_HERMOSA@HOTMAIL.COM', 1, '1986-11-13', 'UCHIZA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (65, 'CASTAÑEDA REATEGUI MEEJHEIRA PAOLA', '73382880', 1, '', '', 'actualizatucorreo@sokso.com', 1, '2000-05-22', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (66, 'cumapa camacho miriam', '43279040', 1, 'uchiza', '', 'actualizatucorreo@sokso.com', 1, '1985-04-02', 'UCHIZA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (67, 'VILLANUEVA CABALLERO AMYLYNN SHOLANSH', '72260106', 1, 'San Juan km4. Uchiza', '', 'Amylynn.asvc@gmail.com', 1, '1994-03-29', 'UCHIZA', 'TOCACHE', 'SAN MARTIN', '2021-03-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (68, 'HERRERA MERINO KITT ROSE', '46009739', 1, 'UCHIZA SAN ANDRES', '', 'actualizatucorreo@sokso.com', 1, '1989-10-12', 'UCHIZA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (69, 'Nodre Aliaga Esther', '33818449', 1, 'Jr.esteban Delgado #111', '', 'Enodrealiaga16@gmail.com', 1, '1974-06-24', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (70, 'CABALLERO ABAD FIYORELY MABEL', '46995051', 1, 'C.P. PARAISO CASERIO SANTA CUY', '', 'rosefiorelle_1991@hotmail.com', 1, '1991-05-22', 'CHOLON', 'MARANON', 'HUANUCO', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (71, 'SANDOVAL DIAZ RAMIRO', '41832167', 1, 'UCHIZA', '', 'ramirosandovaldias1978@gmail.com', 1, '1978-08-16', 'UCHIZA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (72, 'PEREZ VARGAS ANALY MABEL', '41439470', 1, 'Jr. José Olaya cdra 1 con belaunde', '', 'Analymabel76@gmail.com', 1, '1982-06-07', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (73, 'INOCENTE LOPEZ LIDOLITH', '46865978', 1, 'SANTA LUCIA', '', 'actualizatucorreo@sokso.com', 1, '1992-01-26', 'UCHIZA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (74, 'GIRON TENAZOA MARIA VICTORIA', '45510172', 1, 'José María panduro (ref. Frente a l', '', 'Giront21@gmail.com', 1, '1988-09-21', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (75, 'AGUIRRE DOMINGUEZ VELMIRA SILVIA', '02876226', 1, 'Av  Ricardo  palma Cdra 2', '', 'silviavelmira@gmail.com', 1, '1968-05-19', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (76, 'BLANCAS MESA DORITA DAMIANA', '00961851', 1, 'Jr PEDRO Gomez cdra 3 44', '', 'actualizatucorreo@sokso.com', 1, '1964-02-12', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (77, 'CHAVEZ LOPEZ SUHANI IVETH', '44784478', 1, 'km 9 El Porvenir/ carretera Uchiza-', '', 'suhaniveth_2311_87@hotmail.com', 1, '1987-11-23', 'UCHIZA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (78, 'CESPEDES SANCHEZ YESSICA SONIA', '46975607', 1, 'JR. ALAMEDA C-01', '', 'jessicacespedes91@gmail.com', 1, '1991-04-05', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (79, 'BARDALES PUERTA DANITZA', '45152145', 1, 'Jr pedro Gomes Cdra 01', '', 'danitzabp@hotmail.com', 1, '1988-07-12', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (80, 'SORIA CHACHI JHONSI ESTEFFI', '46602486', 1, 'TOCACHE', '', 'jhonsi_25@hotmail.com', 1, '1990-07-28', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (81, 'SANCHEZ JIMENEZ FLOR MILY', '45580151', 1, 'Direccion Tocache viejo barrio Las', '', 'sanchezmily38@gmail.com', 1, '1988-05-15', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (82, 'ALIAGA DIAZ GISELA TATIANA', '47755226', 1, 'TOCACHE', '', 'giche_ad21@hotmail.com', 1, '1990-11-27', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (83, 'CORDOVA TUEROS JEAN PIERRE ADOLFO', '44460699', 1, 'TOCACHE TOCACHE', '', 'actualizatucorreo@sokso.com', 1, '1987-08-03', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (84, 'PISCO ZORRILLA MARY CRIS', '46716985', 1, 'CP. Pucayacu', '', 'crisyt2923@hotmail.com', 1, '1990-08-29', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (85, 'LOPEZ VILLANUEVA KELLY ZOBDI', '75339748', 1, 'VILLA MERCEDES', '', 'kellylopezvillanueva@gmail.com', 1, '1989-01-31', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-03-31', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (86, 'CARRION USQUIANO YANIRA', '75957698', 1, 'Jr. San Juan C.14', '', 'yhacaus@gmail.com', 1, '1994-11-27', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-04-14', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (87, 'RUFASTO LOPEZ LILA', '41045523', 1, 'Jr. nestor chave 314', '', 'xhiomarameza972@gmail.com', 1, '1981-09-11', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-04-23', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (88, 'RIOS ORTIZ MIRIAN LENITH', '72793324', 1, 'SHIRINGAL', '', 'MILE.121219@GMAIL.COM', 1, '1993-12-12', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (89, 'OLASCUAGA MARTINEZ ESTELA VIKI', '46463392', 1, 'Villa Mercedes Jr. German Rengifo C', '', 'Viki_06_1990@hotmail.com', 1, '1990-02-06', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (90, 'idrogo colunche maria berta', '33819112', 1, 'villa palma', '', 'actualizatucorreo@sokso.com', 1, '1974-05-10', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (91, 'HUAYANAY JESUS MIRTA MADELEYNE', '73333437', 1, 'Jr. JOSE GERMANY 421 TOCACHE', '', 'madeley_hj_2@hotmail.com', 1, '1993-01-03', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (92, 'LOGUNES SORIA MEDELI', '42383172', 1, 'JR. ESTEBAN DELGADO C.3', '', 'M_LOGUNES@HOTMAIL.COM', 1, '1984-05-22', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (93, 'fasabi arevalo jennifer', '71741213', 1, 'ramon castilla 392', '', 'jenfra_0595@outlook.com', 1, '1995-05-05', 'TARAPOTO', 'SAN MARTIN', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (94, 'CASTILLO LALANGI TANIA CONSUELO', '71071715', 1, 'Jr conquistadores cdra 02', '', 'tanila_jlo@hotmail.com', 1, '1991-04-19', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (95, 'santos malpartida claudia', '46561377', 1, 'jr amazonas 350', '', 'claudia18091990@gmail.com.pe', 1, '1990-09-18', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (96, 'artica gozales juana silvana', '42819868', 1, 'jr. san juan 230', '', 'actualizatucorreo@sokso.com', 1, '1983-12-28', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (97, 'CARTAGENA AGUILAR LLIMAR', '42447300', 1, 'JR PEDRO GOMEZ 684', '', 'actualizatucorreo@sokso.com', 1, '1984-05-19', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (98, 'RAMIREZ DIAZ MARIA YOLANDA', '07639859', 1, 'PJE LA UNION LT 41', '', 'actualizatucorreo@sokso.com', 1, '1976-08-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (99, 'MEZARINO GARCIA ROSARIO ESTHER', '41623316', 1, 'CASERIO SARITA COLONIA', '', 'actualizatucorreo@sokso.com', 1, '1981-07-21', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (100, 'DELGADO VASQUEZ ROSA', '41375134', 1, 'Jr fredy aliaga cdra 6', '', 'rous1982_dv@hotmail.com', 1, '1982-05-18', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (101, 'AIRA HURTADO NOEMI', '45074478', 1, 'JR PEDRO GOMEZ C 05', '', 'n.airahurtado@hotmail.com', 1, '1988-03-25', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (102, 'FERNANDEZ RUIZ KATIA HELLEN', '48090486', 1, 'Jr Alameda Crda 04', '', 'katiati_1983@hotmail.com', 1, '1993-11-28', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (103, 'PONCE HUAMAN ERIKA', '43721692', 1, 'Jr los conquistares cdra 01', '', 'erikaponhua84@gmail.com', 1, '1984-10-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-04-01', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (104, 'CARDENAS TORRES ERIKA VANESSA', '70782724', 1, 'jr. Freddy aliaga 700', '', 'cardenasv550@gmail.com', 1, '1992-11-11', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-05-28', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (105, 'Cardenas machuca Keila carmi', '45282619', 1, 'Kei.ed.bri@hotmail.com', '', 'Keila_29@hotmail.com', 1, '1900-01-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (106, 'MuÃ±oz Catalan Kely Roxana', '47939446', 1, 'URB. CERCADO JR. PEGRO GOMEZ S/N', '', 'Lavidaesbellax1000@gmail.com', 1, '1992-12-25', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (107, 'ESTRELLA FLORES BRINER', '42157825', 1, 'Jr. Rafael Veintemilla 251 Tocache', '', 'S_alvino@hotmail.com', 1, '1982-04-08', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (108, 'PUTPAÑA CARDENAS FRIDA GISELA', '73595144', 1, 'TOCACHE', '', 'fridacardena02@gmail.com', 1, '1997-03-21', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (109, 'Valderrama meza Llecenia', '41476775', 1, 'Esteban delgado cuadra 21', '', 'llecenia_fabrizio@hotmail.com', 1, '1982-09-14', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (110, 'VILCHEZ CASTILLO ROSABEL', '01140270', 1, 'JR TUPAC AMARU C06', '', 'rosecal7@gmail.com', 1, '1975-07-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (111, 'ROMERO PUJAY BERNARDINA', '76695041', 1, 'PORONGO', '', 'actualizatucorreo@sokso.com', 1, '2020-09-21', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (112, 'CAMONES FERNANDEZ MARITZA ELENA', '42372553', 1, 'Jirón progreso cuadra 1010', '', 'maritzacamones2103@hotmail.com', 1, '2020-03-21', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (113, 'TRUJILLO CONDEZO JUAN CARLOS', '41882977', 1, 'Jr Grimaldo Reategui cdra 03', '', 'actualizatucorreo@sokso.com', 1, '1983-03-18', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (114, 'SOCORRO PALACIOS LLEMPEN MARIA', '01012125', 1, 'PROLONGACION JORGE CHAVEZ 237', '', 'SOCOPAL09@GMAIL.COM', 1, '1967-09-09', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (115, 'QUINTANA BARZOLA YUCELY', '46052702', 1, 'Jr san juan Cra 13', '', 'actualizatucorreo@sokso.com', 1, '1988-06-20', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (116, 'Hualcas Villanueva Eveling', '75341902', 1, 'Jr 28 de julio  crda 06', '', 'Evelinghualcas@gmail.com', 1, '2000-08-08', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (117, 'VEGA PINEDO MARTHA ELENA', '46984392', 1, 'Barrio tupac frente canchita de fut', '', 'Vega_peru@hotmail.com', 1, '1991-03-07', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (118, 'SERIN LAYZA ERLITH', '42660089', 1, 'TOCACHE', '', 'actualizatucorreo@sokso.com', 1, '1984-09-28', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (119, 'NORIEGA BARRIOS MARIBEL', '46494210', 1, 'JR SAN JUAN C/3', '', 'mnoriegabarrios@gmail.com', 1, '1990-02-14', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (120, 'VILLAJUAN DURAND GADITH', '80520283', 1, 'PROLONGACION BOLOGNESI CDRA 02', '', 'tefany2@hotmial.com', 1, '1980-01-19', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-04-17', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (121, 'PUENTE RIVERA DANIEL', '43509771', 1, 'Av Alameda Perú  # 221 Tingo María', '', 'danielpuented@gmail.com', 1, '1983-08-03', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-05-17', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (122, 'Inga Domínguez Nancy Elizabeth', '73568568', 1, 'Jr.malecon jerman aliaga c.2', '', 'nancyelizabetingadominguez@gmail.com', 1, '1900-01-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (123, 'RODRIGO VILCHEZ ROSITA', '48778466', 1, 'POLVORA', '', 'actualizatucorreo@sokso.com', 1, '1992-11-30', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (124, 'SAAVEDRA GARCIA REYNA', '44768059', 1, 'Jr Grimaldo Reategui cdra 09', '', 'charito145@hotmail.com', 1, '1987-12-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (125, 'RAMIREZ MEZA ANGELICA ELIZABETH', '40180446', 1, 'JR Grimaldo Reátegui número 300', '', 'mezaramirezangelicaelizabeth@gmail.com', 1, '1979-10-04', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (126, 'LOZANO ROBALINO OFELIA INGRID', '40359203', 1, 'Av. Aviación 211', '', 'ofeliaingridl@gmail.com', 1, '1979-09-03', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (127, 'Carrion Correa Karin Eliana', '73475865', 1, 'URB. CERCADO JR. PEGRO GOMEZ S/N', '', 'Karincc18@gmail.com', 1, '1996-07-18', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (128, 'CHINCHAY HUAMANI JESSICA', '46971077', 1, 'Jr .Grimaldo Reategui cuadra 3', '', 'jessicachinchayhuamani1@gmail.com', 1, '1992-03-27', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (129, 'HOYOS CARRASCO NERSY', '75516633', 1, 'Jr pedro Gomes Cdra 02', '', 'actualizatucorreo@sokso.com', 1, '1998-06-24', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (130, 'MEZA DOMINGUEZ ELENA MIRTHA', '40820993', 1, 'Tananta', '', 'actualizatucorreo@sokso.com', 1, '1980-08-13', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (131, 'Neira Contreras DOMITILA', '45229706', 1, 'Caserio puerto rico', '', 'sonia_neira82@hotmail.com', 1, '1982-07-01', 'POLVORA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (132, 'SIMON MOYA YETSY NATHALY', '71047499', 1, 'TOCACHE', '', 'yett092@hotmail.com', 1, '1992-12-25', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (133, 'ROSALES MILIAN ARMANDY', '46376803', 1, 'POLVORA', '', 'armandyrosalesmilian123@gmail.com', 1, '1988-08-01', 'POLVORA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (134, 'CORDOVA VILELA NORLY ELINA', '45012645', 1, 'POLVORA', '', 'actualizatucorreo@sokso.com', 1, '1981-11-23', 'POLVORA', 'TOCACHE', 'SAN MARTIN', '2021-01-27', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (135, 'MEDRANO DAVILA KANDY GRACE', '47137819', 1, 'Jr. Pedro gomez cdra 15', '', 'greis_722@hotmail.com', 1, '1991-07-22', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (136, 'ALIAGA ENRRIQUEZ MILAGROS', '47885573', 1, 'Esteban delgado 423', '', 'milagros_aliaga_112@hotmail.com', 1, '1993-03-22', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (137, 'PAZ ROMERO WILMER', '41228621', 1, 'JR Leonardo Hidalgo cdra7', '', 'Janyortiz17@gmail.com', 1, '1982-03-13', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (138, 'PINTADO RUIZ JUANI', '00975635', 1, 'POLVORA', '', 'Katerine0639@hotmail.com', 1, '1974-12-23', 'POLVORA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (139, 'MORENO SANCHEZ ERIKA DIANA', '47440869', 1, 'C.P. NUEVO HORIZONTE', '', 'actualizatucorreo@sokso.com', 1, '1990-11-11', 'POLVORA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (140, 'SANTOS LABAN DIOMERLI', '72512941', 1, 'AA HH LAS FLORES', '', 'misayuri.2010@gmail.com', 1, '1993-10-19', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (141, 'REVIER PINCHE ROSA NELIDA', '22983908', 1, 'VUEVO HORIZONTE', '', 'ronerepi@hotmail.com', 1, '1962-07-23', 'POLVORA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (142, 'PADILLA BACA MARILIN NATALI', '42832282', 1, 'Jr huallaga Cra 02', '', 'natalipadillabaca@gmail.com', 1, '1984-11-08', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (143, 'GUEVARA REATEGUI AYDEE', '42471493', 1, 'Jr chorro san juan cdra 10', '', 'guevara8411@gmail.com', 1, '1984-03-26', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (144, 'MOSQUERA BACA VALERIE JOANNA', '46833248', 1, 'Jr. San Juan 621', '', 'valeriejohana@hotmail.com', 1, '1991-03-25', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (145, 'MARIN OJEDA DIANA ESTHER', '43529478', 1, 'Jr German Rengifo N° 711', '', 'actualizatucorreo@sokso.com', 1, '1986-02-25', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (146, 'RAMOS MELGAREJO MIRIAN ROSARIO', '46549118', 1, 'JR CHORRO SAN JUAN CRA 16', '', 'MIRIANROSARIORAMOSMELGAREJO@GMAIL.COM', 1, '1990-05-11', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (147, 'LOPEZ TORRES ALEJANDRINA SEBASTIANA', '71083296', 1, 'Esteban Delgado Cdra 01', '', 'lopeztorresalejandrina@gmail.com', 1, '1995-02-26', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-02-04', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (148, 'CALDERON RITUAY YHADIRA LISBETH', '75335013', 1, 'CERCA AL IPD FRENTE AL  EX RECREO ANACONDA', '', 'yhadiralcr@gmail.com', 1, '1999-02-13', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-04-08', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (149, 'VALQUI PAREDES MARBHELITH', '42941131', 1, 'JR. Grimaldo Reategui C/217', '', 'marbhelithvp85@gmail.com', 1, '1985-04-15', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-05-01', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (150, 'lopez ocaña lilian elizabeth', '74771358', 1, 'Jr. Tocache cdra 11', '', 'lilianlopezocana@gmail.com', 1, '1994-07-06', 'RUPA RUPA', 'LEONCIO PRADO', 'HUANUCO', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (151, 'ESCOBEDO TUANAMA KATHERIN', '48026066', 1, 'katherin.escobedo@hotmail.com', '', 'katherin.escobedo@hotmail.com', 1, '1983-12-21', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-05-01', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (152, 'SAYAN ROJAS VILMA', '00999465', 1, 'SANTA ROSA DE SHAPAJA', '', 'Yvelasquezs@undac.edu.pe', 1, '1957-10-24', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-05-21', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (153, 'RUIZ LEON LIZ STEPHANY', '46900126', 1, 'TOCACHE', '', 'actualizatucorreo@sokso.com', 1, '1992-03-03', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (154, 'taminche moreno Jhano', '48336420', 1, 'JR CHORRO SAN JUAN CRA 10 1005', '', 'Viajese94@gmail.com', 1, '1994-04-07', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (155, 'MEZA VILLAFUERTE YURIKO CHUYITO', '46801218', 1, 'TOCACHE', '', 'actualizatucorreo@sokso.com', 1, '1990-12-26', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (156, 'GONGORA GAMEZ LIZETH KARINA', '75549961', 1, 'TOCACHE', '', 'actualizatucorreo@sokso.com', 1, '1994-08-14', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (157, 'QUINTEROS RIOS ALEJANDRINA', '09610556', 1, 'JR Leoncio prado c/01 Tupac Amaru', '', 'alejandrinaquinterosrios@gmail.com', 1, '1971-04-04', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (158, 'GALVEZ CHIHUAN LUZ ELENA', '71184303', 1, 'Santo Silva cuadra 1 TOCACHE', '', 'galvezluzelena333@gmail.com', 1, '2002-06-26', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-02-25', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (159, 'HUAMAN SILVA PATROCINIO LENIN', '42215615', 1, 'TOCACHE', '', 'lenin17_01@hotmail.com', 1, '1984-01-17', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-02-27', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (160, 'MILLA GUILLEN DAMARIS', '46789188', 1, 'Jr Micaela Bastida Crda 02', '', 'soniaelvapevi@gmail.com', 1, '1992-01-29', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-03-06', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (161, 'PONTE CARTAGENA NILSA VANESSA', '46033279', 1, 'Caserío Tocache viejo', '', 'Vane17_89@hotmail.com', 1, '1989-01-09', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-04-03', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (162, 'CHUQUICONDOR DE LA CRUZ ISABEL RUTH', '76246783', 1, 'Jr Julio Arevalo Cdra 19', '', 'ruthchquicondor.96@gmail.com', 1, '1996-04-07', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-04-10', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (163, 'PEÑA VILLANUEVA EBERH MARILITH', '43037077', 1, 'Jr. PEDRO GOMES C/03 Jr. PEDRO GOMES C/03', '', 'actualizatucorreo@sokso.com', 1, '2018-10-01', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (164, 'Gomez Luna  Kelly Ruth', '71568166', 1, 'Jr daudilio zavaleta cdr 1 SN', '', 'Marck15zebasthian@hotmail.com', 1, '1996-06-13', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (165, 'HUAMAN SILVA MAGDALENA', '47964571', 1, 'Jr. Chorro San Juan cdr. 17', '', 'magdale.2014@hotmail.com', 1, '1992-10-16', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (166, 'Rojas Valensuela Mercedes', '06980879', 1, 'JR PEDRO GOMES CDRA 16', '', 'actualizatucorreo@sokso.com', 1, '1964-09-24', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (167, 'AREVALO ARAUFO HEIDY LI', '71871962', 1, 'Av Ricardo Palma 1097', '', 'harevaloaraujo@gmail.com', 1, '1995-12-10', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (168, 'BALBIN GOMEZ SARITA', '44049350', 1, 'Jr esteban Delgado Crda 10', '', 'sagobakits1@hotmail.com', 1, '1987-01-06', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (169, 'TRUJILLO MARTINEZ CANDY', '47613248', 1, 'Av. Ricardo palma 486', '', 'Candymartinezrc@gmail.com', 1, '1992-03-31', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (170, 'ORTIZ SHUPINGAHUA EDGAR', '44487473', 1, 'Jr Grimaldo Reategui cdra 02', '', 'ahorrofarma5@gmail.com', 1, '1987-09-04', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (171, 'Lopez guevara Diana isabel', '45740137', 1, 'Bambamarca Bambamarca', '', 'Peter_1987_05@hotmail.es', 1, '2019-02-10', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (172, 'paredes Silva Jenny', '80594294', 1, 'Jr chorro san juan cdra 9. Nº 96', '', 'jeny_1723@hotmail.com', 1, '1975-10-20', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (173, 'LEIVA CUEVA DANNY PATRICIA', '71539631', 1, 'Jr Grimaldo Reategui cdra 02', '', 'dpleiva94@hotmail.com', 1, '1994-10-06', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (174, 'gonzales silva Fredy johan', '42614208', 1, 'TOCACHE', '', 'fredyjohangonzalessilva7@gmail.com', 1, '1985-08-13', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (175, 'FERNANDEZ SANCHEZ ANEYDA', '44357906', 1, 'Jr pedro Gomes 702', '', 'aneyda@gmail.com', 1, '1987-04-07', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 2, '10443579066', 'FERNANDEZ SANCHEZ ANEYDA ', '-');
INSERT INTO `cliente` VALUES (176, 'VELASQUEZ SAYAN YENIFER JHANIRA', '75072260', 1, 'Marginal Fernando Belaunde Terry 3', '', 'Yenifer.24.sayan@gmail.com', 1, '2001-08-24', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '1900-01-01', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (177, 'Matos Aguero Jhoel Gabriel', '71885873', 1, 'Rafael ventemia cuadra 1', '', 'actualizatucorreo@sokso.com', 1, '1994-03-17', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (178, 'PAREDES SILVA YANELY', '42919204', 1, 'AV. AVIACION C.03', '', 'YANPARSIL_33_@HOTMAIL.COM', 1, '1982-12-12', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (179, 'HERRERA GOMEZ CLAUDINA', '42471505', 1, 'Jirón José germán y cdra7', '', 'Herreragimezclaudina@gmail.com', 1, '1984-06-18', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (180, 'PEREZ MENDOZA GELLY', '42963267', 1, 'JR SAN MARTIN CDRA 13', '', 'actualizatucorreo@sokso.com', 1, '1981-12-10', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (181, 'Orihuela Perez Dexi Roxana', '48048585', 1, 'Jr.san Juan cuadra 12', '', 'Deysi_orihuela_33@hotmail.Com', 1, '1987-09-02', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (182, 'MORE SEMINARIO JEANETTE MARIBEL', '41195516', 1, 'TOCACHE', '', 'Jeanettms_1@hotmail.com', 1, '1981-11-04', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (183, 'Machoa Linares Marcia Natalia', '71010913', 1, 'Prolongación Jorge Chávez cuadra 2', '', 'marciaescorpio.93@gmail.com', 1, '1993-10-28', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-03-12', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (184, 'AGUILAR CARTAGENA RENAN', '20105612', 1, 'JR JACINTACARTAGENA 279', '', 'raguilar@coopactocache.com.pe', 1, '1972-08-24', 'TOCACHE', 'TOCACHE', 'SAN MARTIN', '2021-06-18', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (185, 'ROJAS GARCIA LESLYE ROSMERY', '77564551', 1, 'Jirón la merced 160', '', 'scorpio.lrrg@gmail.com', 1, '1995-11-07', 'JUANJUI', 'MARISCAL CACERES', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (186, 'del castillo vasquez Astrid karolina', '77287277', 1, 'Astrid karolina del castillo vasque z', '', 'Jhoashin1311@gmail.com', 1, '1995-04-04', 'JUANJUI', 'MARISCAL CACERES', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (187, 'BRIOSO COTRINO SUDY  SADAY', '46512455', 1, 'POLVORA', '', 'Azulbc21@gmail.com', 1, '1990-07-19', 'POLVORA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (188, 'alamo lopez orler delmer', '70765162', 1, 'nuevo horizonte', '', 'actualizatucorreo@sokso.com', 1, '1998-03-07', 'POLVORA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);
INSERT INTO `cliente` VALUES (189, 'PINTADO KATERINE RUT KATERINE', '72321998', 1, 'POLVORA', '', 'Katerine0639@hotmail.com', 1, '1994-12-05', 'POLVORA', 'TOCACHE', 'SAN MARTIN', '2021-01-24', 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for detalle_venta
-- ----------------------------
DROP TABLE IF EXISTS `detalle_venta`;
CREATE TABLE `detalle_venta`  (
  `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `cantidad` decimal(11, 2) NULL DEFAULT NULL,
  `precio` decimal(10, 2) NULL DEFAULT NULL,
  `producto_id` int(255) NULL DEFAULT NULL,
  `id_venta` int(11) NULL DEFAULT NULL,
  `observaciones` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_venta_marca` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_venta_modelo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_venta_color` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_venta_familia` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_venta_categoria` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_venta_tipo_marca` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_venta_submarca` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_venta_talla` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_venta_documento` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_codigo_producto` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_fecha_venta` date NULL DEFAULT NULL,
  `detalle_cod_producto` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_tipo_documento` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `detalle_documento_identidad` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_detalle_venta`) USING BTREE,
  INDEX `fk_venta_detalle`(`id_venta`) USING BTREE,
  INDEX `fk_ventas_producto`(`producto_id`) USING BTREE,
  CONSTRAINT `detalle_venta_ibfk_1` FOREIGN KEY (`id_venta`) REFERENCES `venta` (`venta_idventas`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `detalle_venta_ibfk_2` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`producto_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 41692 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for documento
-- ----------------------------
DROP TABLE IF EXISTS `documento`;
CREATE TABLE `documento`  (
  `id_documento` int(1) NOT NULL AUTO_INCREMENT,
  `doc_serie` char(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `doc_correlativo` int(11) NULL DEFAULT NULL,
  `id_empresa` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_tipodocumento` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_documento`) USING BTREE,
  INDEX `fk_tipodoc`(`id_tipodocumento`) USING BTREE,
  CONSTRAINT `documento_ibfk_1` FOREIGN KEY (`id_tipodocumento`) REFERENCES `tipo_documento` (`tipodoc_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of documento
-- ----------------------------
INSERT INTO `documento` VALUES (1, 'B001', '1', 7773, '20493806077', 2);
INSERT INTO `documento` VALUES (2, 'F001', '1', 553, '20493806077', 1);
INSERT INTO `documento` VALUES (3, '001', '1', 299, '20493806077', 6);
INSERT INTO `documento` VALUES (4, 'T001', '1', 23, '20493806077', 7);

-- ----------------------------
-- Table structure for empleados
-- ----------------------------
DROP TABLE IF EXISTS `empleados`;
CREATE TABLE `empleados`  (
  `empleado_id` int(11) NOT NULL AUTO_INCREMENT,
  `empleado_nombres` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `empleado_apellidos` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `empleado_dni` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `empleado_direccion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `empleado_email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `empleado_telefono` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `perfil_id` int(11) NULL DEFAULT NULL,
  `empleado_usuario` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `empleado_clave` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` int(1) NULL DEFAULT 1,
  `empleado_nombre_completo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `empleado_foto_perfil` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `empleado_fecha_nacimiento` date NULL DEFAULT NULL,
  `empleado_fecha_ingreso` date NULL DEFAULT NULL,
  `empleado_fecha_salida` date NULL DEFAULT NULL,
  `empleado_sueldoplanilla` decimal(10, 2) NULL DEFAULT NULL,
  `empleado_sueldoreal` decimal(10, 2) NULL DEFAULT NULL,
  `empleado_fondo_pension` decimal(10, 2) NULL DEFAULT NULL,
  `id_fondo_pension` int(11) NULL DEFAULT NULL,
  `empleado_cci` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `empleado_numbanco` varchar(14) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `empleado_idbanco` int(11) NULL DEFAULT NULL,
  `empleado_sexo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'm',
  `empleado_idsalud` int(11) NULL DEFAULT NULL,
  `empleado_horario_entrada_man` time(0) NULL DEFAULT NULL,
  `empleado_horario_salida_man` time(0) NULL DEFAULT NULL,
  `empleado_horario_entrada_tar` time(0) NULL DEFAULT NULL,
  `empleado_horario_salida_tar` time(0) NULL DEFAULT NULL,
  `empleado_inicio_contrato` date NULL DEFAULT NULL,
  `empleado_fecha_inicio` date NULL DEFAULT NULL,
  `empleado_estado_planilla` int(1) NULL DEFAULT 0,
  `empresa_sede` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`empleado_id`) USING BTREE,
  INDEX `fk_perfil_empleado`(`perfil_id`) USING BTREE,
  INDEX `id_fondo_pension`(`id_fondo_pension`) USING BTREE,
  INDEX `empleados_ibfk_bancos`(`empleado_idbanco`) USING BTREE,
  INDEX `empleados_ibfk_salud`(`empleado_idsalud`) USING BTREE,
  INDEX `empleados_ibfk_3`(`empresa_sede`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of empleados
-- ----------------------------
INSERT INTO `empleados` VALUES (1, 'admin', 'admin', '75016083', '-', 'admin@gmail.com', '933122626', 12, 'admin', '123', 1, 'admin Salas Romero', 'default.jpg', '1996-04-03', '2019-05-11', NULL, 2000.00, 2000.00, NULL, NULL, NULL, NULL, NULL, 'm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1);

-- ----------------------------
-- Table structure for formapago
-- ----------------------------
DROP TABLE IF EXISTS `formapago`;
CREATE TABLE `formapago`  (
  `for_id` int(11) NOT NULL AUTO_INCREMENT,
  `for_descripcion` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `for_estado` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`for_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of formapago
-- ----------------------------
INSERT INTO `formapago` VALUES (1, 'Efectivo', 1);
INSERT INTO `formapago` VALUES (2, 'POS VISA', 1);
INSERT INTO `formapago` VALUES (3, 'VISA', 1);
INSERT INTO `formapago` VALUES (4, 'CHECKE', 1);

-- ----------------------------
-- Table structure for modulos
-- ----------------------------
DROP TABLE IF EXISTS `modulos`;
CREATE TABLE `modulos`  (
  `modulo_id` int(11) NOT NULL AUTO_INCREMENT,
  `modulo_nombre` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modulo_icono` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modulo_url` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modulo_padre` int(11) NULL DEFAULT NULL,
  `estado` int(1) NULL DEFAULT 1,
  `modulo_orden` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`modulo_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of modulos
-- ----------------------------
INSERT INTO `modulos` VALUES (1, 'MODULO PADRE', '#', '#', 1, 1, 1);
INSERT INTO `modulos` VALUES (2, 'Seguridad', 'fa fa-key', '.', 1, 1, 5);
INSERT INTO `modulos` VALUES (3, 'Permisos', ' ', 'Permisos', 2, 1, NULL);
INSERT INTO `modulos` VALUES (4, 'Perfiles', ' ', 'Perfiles', 2, 1, 4);
INSERT INTO `modulos` VALUES (5, 'Modulos', ' ', 'Modulo', 2, 1, 6);
INSERT INTO `modulos` VALUES (6, 'Empresa', '.', 'empresa', 2, 0, NULL);
INSERT INTO `modulos` VALUES (7, 'Almacen', 'fa fa-inbox', '#', 1, 1, 6);
INSERT INTO `modulos` VALUES (8, 'R. de Almacen', ' ', 'almacen', 7, 1, NULL);
INSERT INTO `modulos` VALUES (9, 'Mantenimiento', 'fa fa-cog', '#', 1, 1, 4);
INSERT INTO `modulos` VALUES (10, 'Categoria Producto', ' ', 'C_producto', 9, 1, NULL);
INSERT INTO `modulos` VALUES (11, 'Marca Producto', ' ', 'Marca_producto', 9, 1, NULL);
INSERT INTO `modulos` VALUES (12, 'Tipo Documento', ' ', 'Tipo_documento', 9, 1, NULL);
INSERT INTO `modulos` VALUES (13, 'Tipo Moneda', ' ', 'Tipo_moneda', 9, 1, NULL);
INSERT INTO `modulos` VALUES (14, 'Unidad Medida', ' ', 'Unidad_medida', 9, 0, NULL);
INSERT INTO `modulos` VALUES (15, 'Compras', 'fa fa-cart-arrow-down ', '#', 1, 1, 3);
INSERT INTO `modulos` VALUES (16, 'Registrar Producto', ' ', 'R_producto', 15, 1, NULL);
INSERT INTO `modulos` VALUES (17, 'Ubicación Mesa', NULL, 'Ubicacionmesa', 9, 0, NULL);
INSERT INTO `modulos` VALUES (18, 'Proveedor', '.', 'Proveedor', 15, 1, NULL);
INSERT INTO `modulos` VALUES (19, 'Registro compra', '.', 'Compra', 15, 1, NULL);
INSERT INTO `modulos` VALUES (20, 'Mesas', ' ', 'Mesas', 9, 0, NULL);
INSERT INTO `modulos` VALUES (21, 'Ventas', 'fa fa-shopping-bag', '.', 1, 1, 1);
INSERT INTO `modulos` VALUES (22, 'Venta Mesa', ' ', 'Venta_mesa', 21, 0, NULL);
INSERT INTO `modulos` VALUES (23, 'Caja', 'fa fa-cc', '.', 1, 1, 2);
INSERT INTO `modulos` VALUES (24, 'Sesión Caja', ' ', 'Sesion_caja', 23, 1, NULL);
INSERT INTO `modulos` VALUES (25, 'Registro Comprobantes', ' ', 'Registro_comprobante', 9, 1, NULL);
INSERT INTO `modulos` VALUES (26, 'Asignar Comprobantes', ' ', 'Asignar_comprobantes', 9, 0, NULL);
INSERT INTO `modulos` VALUES (27, 'Pedido', ' ', 'Pedido', 23, 0, NULL);
INSERT INTO `modulos` VALUES (28, 'Empleados', 'fa fa-user-circle', '#', 1, 1, 5);
INSERT INTO `modulos` VALUES (29, 'Empleados', ' ', 'Usuario', 28, 1, NULL);
INSERT INTO `modulos` VALUES (30, 'Concepto', ' ', 'Concepto', 23, 1, NULL);
INSERT INTO `modulos` VALUES (31, 'Gestión de Movimientos', ' ', 'movimiento', 23, 1, NULL);
INSERT INTO `modulos` VALUES (32, 'Cobro de Venta al Credito', ' ', 'Venta_credito', 21, 1, NULL);
INSERT INTO `modulos` VALUES (33, 'Reportes', 'fa fa-file-pdf', '#', 1, 1, 7);
INSERT INTO `modulos` VALUES (34, 'Rep. Grafico Ventas', ' ', 'Reporte_venta', 33, 1, NULL);
INSERT INTO `modulos` VALUES (35, 'Gráfico de VentaxEmpleados', ' ', 'Venta_empleado', 33, 1, NULL);
INSERT INTO `modulos` VALUES (36, 'Platos Vendidos', ' ', 'R_plato_vendido', 33, 0, NULL);
INSERT INTO `modulos` VALUES (37, 'Productos Cancelados', ' ', 'R_roducto_cancelado', 33, 0, NULL);
INSERT INTO `modulos` VALUES (38, 'Clientes', ' ', 'Clientes', 21, 1, NULL);
INSERT INTO `modulos` VALUES (39, 'Registrar Plato', '.', 'Registrar_plato', 21, 0, NULL);
INSERT INTO `modulos` VALUES (40, 'Cocina', 'fa fa-fire', '#', 1, 0, NULL);
INSERT INTO `modulos` VALUES (41, 'Cocina', '.', 'Cocina', 40, 0, NULL);
INSERT INTO `modulos` VALUES (42, 'Produccion', '.', 'Produccion', 40, 1, NULL);
INSERT INTO `modulos` VALUES (43, 'Historia de Sesión Caja', ' ', 'HSesion_caja', 23, 1, NULL);
INSERT INTO `modulos` VALUES (44, 'Mostrador', '.', 'Mostrador', 21, 0, NULL);
INSERT INTO `modulos` VALUES (45, 'Lista Ventas', '.', 'ventas', 21, 1, NULL);
INSERT INTO `modulos` VALUES (46, 'Ventas', '.', 'Venta', 21, 1, NULL);
INSERT INTO `modulos` VALUES (47, 'proforma', '.', 'Proforma', 21, 1, NULL);
INSERT INTO `modulos` VALUES (48, 'Guía de Remisión ', '.', 'Guia_remision', 21, 1, NULL);
INSERT INTO `modulos` VALUES (49, 'Proforma', '.', 'Proforma', 21, 1, NULL);
INSERT INTO `modulos` VALUES (50, 'Reporte venta', '.', 'Reporteventa', 33, 1, NULL);

-- ----------------------------
-- Table structure for perfiles
-- ----------------------------
DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE `perfiles`  (
  `perfil_id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_descripcion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` int(1) NULL DEFAULT 1,
  `perfil_url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`perfil_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of perfiles
-- ----------------------------
INSERT INTO `perfiles` VALUES (1, 'ADMINISTRADOR', 1, '');
INSERT INTO `perfiles` VALUES (2, 'MOZO', 0, 'Venta_mesa');
INSERT INTO `perfiles` VALUES (5, 'CAJERO', 1, 'Venta');
INSERT INTO `perfiles` VALUES (6, 'COCINERO', 0, 'cocina');
INSERT INTO `perfiles` VALUES (8, 'ADMINISTRADOR DE EMPRESA', 1, 'Venta');
INSERT INTO `perfiles` VALUES (12, 'ADMINISTRADOR DE SEDE', 1, 'Control');
INSERT INTO `perfiles` VALUES (13, 'DELIVERISTA', 0, 'Delivery');
INSERT INTO `perfiles` VALUES (14, 'CARMEN', 1, NULL);

-- ----------------------------
-- Table structure for permisos
-- ----------------------------
DROP TABLE IF EXISTS `permisos`;
CREATE TABLE `permisos`  (
  `perfil_id` int(11) NULL DEFAULT NULL,
  `modulo_id` int(11) NULL DEFAULT NULL,
  `empresa_ruc` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_tipo_negocio` int(11) NULL DEFAULT NULL,
  INDEX `fk_modulo_permiso`(`modulo_id`) USING BTREE,
  INDEX `fk_perfil_permiso`(`perfil_id`) USING BTREE,
  INDEX `permisos_ibfk_3`(`id_tipo_negocio`) USING BTREE,
  CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`perfil_id`) REFERENCES `perfiles` (`perfil_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for producto
-- ----------------------------
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto`  (
  `producto_id` int(255) NOT NULL,
  `producto_codigo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `producto_descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `producto_codigo_barras` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `producto_precio` double(15, 2) NULL DEFAULT NULL,
  `producto_estado` int(255) NULL DEFAULT NULL,
  `producto_talla` int(255) NULL DEFAULT NULL,
  `producto_familia` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`producto_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tabla_inicial
-- ----------------------------
DROP TABLE IF EXISTS `tabla_inicial`;
CREATE TABLE `tabla_inicial`  (
  `tabla_inicial_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_inicial_cod_prom` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_dni` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_campania` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_codigo_producto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_descripcion_producto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_marca` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `table_inicial_modelo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_genero` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_familia` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_categoria` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_tipo_marca` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_sub_categoria` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_talla` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_cantidad` int(255) NULL DEFAULT NULL,
  `tabla_inicial_fecha` date NULL DEFAULT NULL,
  `tabla_inicial_precio` double(12, 2) NULL DEFAULT NULL,
  `tabla_inicial_subtotal` double(12, 2) NULL DEFAULT NULL,
  `tabla_inicial_igv` double(12, 2) NULL DEFAULT NULL,
  `tabla_inicial_total` double(12, 2) NULL DEFAULT NULL,
  `tabla_inicial_precio_promocion` double(12, 2) NULL DEFAULT NULL,
  `tabla_inicial_subtotal_promocion` double(12, 2) NULL DEFAULT NULL,
  `tabla_inicial_igv_promocion` double(12, 2) NULL DEFAULT NULL,
  `tabla_inicial_total_promocion` double(12, 2) NULL DEFAULT NULL,
  `tabla_inicial_tipo_documento` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_num_documento` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_origin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_t_doc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_t_numero` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_t_doc_ref` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_t_serie_ref` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tabla_inicial_t_numero_ref` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`tabla_inicial_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42565 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tabla_inicial
-- ----------------------------
INSERT INTO `tabla_inicial` VALUES (41953, '1200295731', '41733632', 'JOBANI VASQUEZ DEL CASTILLO', 'DAM21OINS04', '2000019902', 'ZAPATILLAS PR URBANO NINA SIMIK Q5-075', 'NINA SIMIK', 'Q5-075            ', 'CELESTE', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'NINA SIMIK', 'URBANO', '36', -1, '2021-06-16', 39.66, -39.66, -7.14, -46.80, 43.85, -43.85, -7.89, -51.74, 'Nota Credito', '91275192', '', 'ZNC1', '0FC20 0263216', '', '0F020', '0416554');
INSERT INTO `tabla_inicial` VALUES (41954, '1200050585', '01013477', 'GLADIS AREVALO RODRIGUEZ', 'OFT21BR0005', '2000020396', 'SANDALIAS TIJERA GRENDHA 2GDA31', 'GRENDHA', '2GDA31            ', 'ROSA', 'MUJER', 'BASICO', 'SANDALIAS', 'LICENCIA', 'TIJERA', '36', -1, '2021-06-16', 29.16, -29.16, -5.25, -34.41, 31.36, -31.36, -5.65, -37.01, 'Nota Credito', '91275184', '', 'ZNC1', '0FC20 0263215', '', '0F020', '0415895');
INSERT INTO `tabla_inicial` VALUES (41955, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'DAM21OINS04', '2000019902', 'ZAPATILLAS PR URBANO NINA SIMIK Q5-075', 'NINA SIMIK', 'Q5-075            ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'NINA SIMIK', 'URBANO', '38', -1, '2021-06-16', 39.66, -39.66, -7.14, -46.80, 43.85, -43.85, -7.89, -51.74, 'Nota Credito', '91275195', '', 'ZNC1', '0FC20 0263218', '', '0F020', '0415895');
INSERT INTO `tabla_inicial` VALUES (41956, '1200251724', '43969035', 'KARIN SHEYLA DEL AGUILA ROJAS', 'CAB21DP0001', '2000020701', 'BOTAS OUTDOOR AMADEO ASTO GGW-015', 'AMADEO ASTO', 'GGW-015           ', 'AFRICANO', 'HOMBRE', 'BASICO', 'BOTAS', 'AMADEO ASTO', 'OUTDOOR', '42', -1, '2021-06-16', 159.41, -159.41, -28.69, -188.10, 177.33, -177.33, -31.92, -209.25, 'Nota Credito', '91275183', '', 'ZNC1', '0FC20 0263214', '', '0F020', '0417385');
INSERT INTO `tabla_inicial` VALUES (41965, '1200326589', '75335013', 'YHADIRA LISBETH CALDERON RITUAY', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'NEGRO', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -1, '2021-06-16', 31.27, -31.27, -5.63, -36.90, 34.64, -34.64, -6.24, -40.88, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (41966, '1200326589', '75335013', 'YHADIRA LISBETH CALDERON RITUAY', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'NEGRO', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -2, '2021-06-16', 31.28, -62.55, -11.26, -73.81, 34.64, -69.28, -12.47, -81.75, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (41967, '1200326589', '75335013', 'YHADIRA LISBETH CALDERON RITUAY', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'AZUL', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -2, '2021-06-16', 31.28, -62.55, -11.26, -73.81, 34.64, -69.28, -12.47, -81.75, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (41968, '1200325192', '71010913', 'MARCIA NATALIA MACHOA LINARES', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'AZUL', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -1, '2021-06-16', 31.27, -31.27, -5.63, -36.90, 34.64, -34.64, -6.24, -40.88, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (41969, '1200325192', '71010913', 'MARCIA NATALIA MACHOA LINARES', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'NEGRO', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -1, '2021-06-16', 31.27, -31.27, -5.63, -36.90, 34.64, -34.64, -6.24, -40.88, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (41970, '1200288057', '33819205', 'EMILIA AGUIRRE MALQUI', 'DAM21OISK04', '2000019646', 'ZAPATILLAS PR URBANO SOKSO ADO-185', 'SOKSO', 'ADO-185           ', 'ACERO', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'SOKSO', 'URBANO', '37', -1, '2021-06-16', 51.10, -51.10, -9.20, -60.30, 56.56, -56.56, -10.18, -66.74, 'Nota Credito', '91275193', '', 'ZNC1', '0FC20 0263217', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (41978, '1200312356', '47554312', 'ERLINDA HERNANDEZ CAMPOS', 'KID21OI0003', '2000020753', 'SANDALIAS PULSERA JACK & JACKIE PAS-303', 'JACK & JACKIE', 'PAS-303           ', 'CELESTE', 'GIRLS', 'BASICO', 'SANDALIAS', 'JACK & JACKIE', 'PULSERA', '30', -1, '2021-06-01', 51.10, -51.10, -9.20, -60.30, 56.56, -56.56, -10.18, -66.74, 'Nota Credito', '91266227', '', 'ZNC1', '0FC20 0259512', '', '0F020', '0409326');
INSERT INTO `tabla_inicial` VALUES (41979, '1200251724', '43969035', 'KARIN SHEYLA DEL AGUILA ROJAS', 'CAB21OI0003', '2100005045', 'TOPS CAMISA ML URBAN CREEDS CDR-358', 'URBAN CREEDS', 'CDR-358           ', 'ROJO/BLANCO', 'HOMBRE', 'BASICO', 'TOPS', 'URBAN CREEDS', 'CAMISA ML', 'XL', -1, '2021-06-01', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91266246', '', 'ZNC1', '0FC20 0259517', '', '0F020', '0411836');
INSERT INTO `tabla_inicial` VALUES (41980, '1200296649', '46494210', 'MARIBEL NORIEGA BARRIOS', 'TMP21UD0017', '2000015709', 'ZAPATOS BALERINA MOLECA 5301309', 'MOLECA', '5301309           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'BALERINAS', '36', -1, '2021-06-01', 29.16, -29.16, -5.25, -34.41, 31.36, -31.36, -5.65, -37.01, 'Nota Credito', '91266234', '', 'ZNC1', '0FC20 0259513', '', '0F020', '0407747');
INSERT INTO `tabla_inicial` VALUES (41981, '1200288057', '33819205', 'EMILIA AGUIRRE MALQUI', 'DAM21DMSK01', '2000016946', 'ZAPATILLAS PR URBANO SOKSO ADO-149', 'SOKSO', 'ADO-149           ', 'AZUL', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'SOKSO', 'URBANO', '39', -1, '2021-06-01', 45.00, -45.00, -8.10, -53.10, 50.21, -50.21, -9.04, -59.25, 'Nota Credito', '91266248', '', 'ZNC1', '0FC20 0259519', '', '0F020', '0407880');
INSERT INTO `tabla_inicial` VALUES (41982, '1200291575', '80594294', 'JENNY PAREDES SILVA', 'OFT21BR0005', '2000011648', 'ZAPATOS REYNA BEIRA RIO 4076150', 'BEIRA RIO', '4076150           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'REYNA', '36', -1, '2021-06-01', 53.59, -53.59, -9.65, -63.24, 57.31, -57.31, -10.32, -67.63, 'Nota Credito', '91266249', '', 'ZNC1', '0FC20 0259520', '', '0F020', '0412553');
INSERT INTO `tabla_inicial` VALUES (41983, '1200315420', '45010411', 'MARIA HERNANDEZ CAMPOS', 'KID21OI0003', '2000021242', 'ZAPATILLAS CROCS MICKEY MOUSE 2MC493', 'MICKEY MOUSE', '2MC493            ', 'AZUL', 'BOYS', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'CROCS', '26', -1, '2021-06-01', 46.50, -46.50, -8.37, -54.87, 49.70, -49.70, -8.95, -58.65, 'Nota Credito', '91266226', '', 'ZNC1', '0FC20 0259511', '', '0F020', '0408601');
INSERT INTO `tabla_inicial` VALUES (41984, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'TMP21UD0019', '2000018374', 'ZAPATOS OXFORD VIALE RUT-5068', 'VIALE', 'RUT-5068          ', 'BRONCE', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'OXFORD', '36', -1, '2021-06-01', 162.36, -162.36, -29.23, -191.59, 174.72, -174.72, -31.45, -206.17, 'Nota Credito', '91266243', '', 'ZNC1', '0FC20 0259514', '', '0F020', '0411146');
INSERT INTO `tabla_inicial` VALUES (41985, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'KID21OI0003', '2100005027', 'BOTTOMS JOGGER JACK & JACKIE KJG-11', 'JACK & JACKIE', 'KJG-11            ', 'VERDE MILITAR', 'BOYS', 'BASICO', 'BOTTOMS', 'JACK & JACKIE', 'JOGGER', '14', -1, '2021-06-01', 45.00, -45.00, -8.10, -53.10, 50.21, -50.21, -9.04, -59.25, 'Nota Credito', '91266245', '', 'ZNC1', '0FC20 0259516', '', '0F020', '0411351');
INSERT INTO `tabla_inicial` VALUES (41986, '1200303106', '47939446', 'KELY ROXANA MUÃ±OZ CATALAN', 'DAM21OISK04', '2000019628', 'SANDALIAS PULSERA SOKSO MNG-015', 'SOKSO', 'MNG-015           ', 'NEGRO', 'MUJER', 'BASICO', 'SANDALIAS', 'SOKSO', 'PULSERA', '35', -1, '2021-06-01', 73.98, -73.98, -13.32, -87.30, 81.99, -81.99, -14.76, -96.75, 'Nota Credito', '91266247', '', 'ZNC1', '0FC20 0259518', '', '0F020', '0412553');
INSERT INTO `tabla_inicial` VALUES (41987, '1200303123', '47613248', 'CANDY TRUJILLO MARTINEZ', 'TMP21UD0016', '2000020589', 'SANDALIAS PULSERA ZAXY 2GZ969', 'ZAXY', '2GZ969            ', 'NEGRO', 'MUJER', 'BASICO', 'SANDALIAS', 'LICENCIA', 'PULSERA', '35', -1, '2021-06-01', 33.89, -33.89, -6.10, -39.99, 36.52, -36.52, -6.57, -43.09, 'Nota Credito', '91266225', '', 'ZNC1', '0FC20 0259510', '', '0F020', '0406626');
INSERT INTO `tabla_inicial` VALUES (41988, '1200291575', '80594294', 'JENNY PAREDES SILVA', 'DAM21RFES03', '2000020598', 'ZAPATILLAS PR URBANO ESTELA SOKSO AL-08', 'ESTELA SOKSO', 'AL-08             ', 'TURQUEZA', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'ESTELA SOKSO', 'LIFESTYLE', '37', -1, '2021-06-01', 79.33, -79.33, -14.28, -93.61, 88.35, -88.35, -15.90, -104.25, 'Nota Credito', '91266244', '', 'ZNC1', '0FC20 0259515', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (41989, '1200050585', '01013477', 'GLADIS AREVALO RODRIGUEZ', 'KID21KL0003', '2000017963', 'ZAPATILLAS CAÑA BAJA KLIN KL-438', 'KLIN', 'KL-438            ', 'NEGRO/BLANCO', 'INFANTE GIRL', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'BAJA', '26', -1, '2021-06-01', 53.59, -53.59, -9.65, -63.24, 57.31, -57.31, -10.32, -67.63, 'Nota Credito', '91266244', '', 'ZNC1', '0FC20 0259515', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (42127, '1200326941', '75957698', 'YANIRA CARRION USQUIANO', 'OFT21EF0003', '2000013465', 'ZAPATILLAS LIFESTYLE FILA 51U326X', 'FILA', '51U326X           ', 'MARINO/BLANCO', 'MUJER', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'LIFESTYLE', '38', -1, '2021-06-15', 119.79, -119.79, -21.56, -141.35, 129.11, -129.11, -23.24, -152.35, 'Nota Credito', '91273883', '', 'ZNC1', '0FC20 0262643', '', '0F020', '0415348');
INSERT INTO `tabla_inicial` VALUES (42128, '1200316234', '46995051', 'FIYORELY MABEL CABALLERO ABAD', 'DAM21OINS04', '2000014220', 'ZAPATILLAS RUNNING OLK DAY-547', 'OLIMPIKUS', 'DAY-547           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'RUNNING', '39', -1, '2021-06-15', 133.19, -133.19, -23.97, -157.16, 143.34, -143.34, -25.80, -169.14, 'Nota Credito', '91273872', '', 'ZNC1', '0FC20 0262632', '', '0F020', '0412554');
INSERT INTO `tabla_inicial` VALUES (42129, '1200050585', '01013477', 'GLADIS AREVALO RODRIGUEZ', 'DAM21OISK04', '2000019633', 'ZAPATOS MOCASIN SOKSO HAR-1813', 'SOKSO', 'HAR-1813          ', 'MOSTAZA', 'MUJER', 'BASICO', 'ZAPATOS', 'SOKSO', 'MOCASIN', '37', -1, '2021-06-15', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91273872', '', 'ZNC1', '0FC20 0262632', '', '0F020', '0412554');
INSERT INTO `tabla_inicial` VALUES (42130, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'OFT21VH0006', '2000018707', 'ZAPATOS PULSERA PIAZZA MULTI-259', 'PIAZZA', 'MULTI-259         ', 'BEIGE', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'PULSERA', '37', -1, '2021-06-15', 89.85, -89.85, -16.17, -106.02, 96.90, -96.90, -17.44, -114.34, 'Nota Credito', '91273882', '', 'ZNC1', '0FC20 0262642', '', '0F020', '0414614');
INSERT INTO `tabla_inicial` VALUES (42131, '1200329534', '70782724', 'ERIKA VANESSA CARDENAS TORRES', 'OFT21OF0008', '2000018474', 'SANDALIAS SLIDE SOKSO CAM-111', 'SOKSO', 'CAM-111           ', 'NEGRO', 'MUJER', 'BASICO', 'SANDALIAS', 'SOKSO', 'SLIDE', '37', -1, '2021-06-15', 45.00, -45.00, -8.10, -53.10, 50.21, -50.21, -9.04, -59.25, 'Nota Credito', '91273884', '', 'ZNC1', '0FC20 0262644', '', '0F020', '0415895');
INSERT INTO `tabla_inicial` VALUES (42132, '1200326941', '75957698', 'YANIRA CARRION USQUIANO', 'OFT21BR0005', '2000020377', 'ZAPATILLAS URBANO BEIRA RIO 41941316', 'BEIRA RIO', '41941316          ', 'BLANCO', 'MUJER', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'URBANO', '36', -1, '2021-06-15', 65.42, -65.42, -11.78, -77.20, 70.63, -70.63, -12.71, -83.34, 'Nota Credito', '91273875', '', 'ZNC1', '0FC20 0262635', '', '0F020', '0413855');
INSERT INTO `tabla_inicial` VALUES (42133, '1200269045', '72793324', 'MIRIAN LENITH RIOS ORTIZ', 'OFT21VH0006', '2000018939', 'ZAPATILLAS RUNNING ROUTT ZUN-015', 'ROUTT', 'ZUN-015           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATILLAS', 'ROUTT', 'RUNNING', '36', -1, '2021-06-15', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91273876', '', 'ZNC1', '0FC20 0262636', '', '0F020', '0414613');
INSERT INTO `tabla_inicial` VALUES (42134, '1200269045', '72793324', 'MIRIAN LENITH RIOS ORTIZ', 'OFT21EF0003', '2000014951', 'ZAPATILLAS RUNNING FILA 11J615X', 'FILA', '11J615X           ', 'NEGRO', 'HOMBRE', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'RUNNING', '38', -1, '2021-06-15', 59.90, -59.90, -10.78, -70.68, 64.30, -64.30, -11.57, -75.87, 'Nota Credito', '91273876', '', 'ZNC1', '0FC20 0262636', '', '0F020', '0414613');
INSERT INTO `tabla_inicial` VALUES (42135, '1200291896', '01012125', 'MARIA SOCORRO PALACIOS LLEMPEN', 'KID21OI0003', '2000021229', 'ZAPATILLAS BAJA PRINCESAS 259-93561', 'PRINCESAS', '259-93561         ', 'CELESTE', 'GIRLS', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'BAJA', '29', -1, '2021-06-15', 33.11, -33.11, -5.96, -39.07, 35.30, -35.30, -6.35, -41.65, 'Nota Credito', '91273876', '', 'ZNC1', '0FC20 0262636', '', '0F020', '0414613');
INSERT INTO `tabla_inicial` VALUES (42142, '1200284793', '07639859', 'MARIA YOLANDA RAMIREZ DIAZ', 'TMP21UD0020', '2100004395', 'TOPS POLO MC FILA LS180387', 'FILA', 'LS180387          ', 'BLANCO', 'HOMBRE', 'BASICO', 'TOPS', 'LICENCIA', 'POLO MC', 'XL', -1, '2021-06-15', 29.16, -29.16, -5.25, -34.41, 31.36, -31.36, -5.65, -37.01, 'Nota Credito', '91273879', '', 'ZNC1', '0FC20 0262639', '', '0F020', '0414532');
INSERT INTO `tabla_inicial` VALUES (42161, '1200251724', '43969035', 'KARIN SHEYLA DEL AGUILA ROJAS', 'DAM21OISK04', '2000019633', 'ZAPATOS MOCASIN SOKSO HAR-1813', 'SOKSO', 'HAR-1813          ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS', 'SOKSO', 'MOCASIN', '36', -1, '2021-06-15', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91273880', '', 'ZNC1', '0FC20 0262640', '', '0F020', '0413244');
INSERT INTO `tabla_inicial` VALUES (42162, '1200307098', '41228621', 'WILMER PAZ ROMERO', 'CAB21OI0003', '2000021195', 'SANDALIAS PULSERA RIDER 2RDA77', 'RIDER', '2RDA77            ', 'NEGRO', 'HOMBRE', 'BASICO', 'SANDALIAS', 'LICENCIA', 'PULSERA', '41', -1, '2021-06-15', 52.81, -52.81, -9.51, -62.32, 56.91, -56.91, -10.24, -67.15, 'Nota Credito', '91273878', '', 'ZNC1', '0FC20 0262638', '', '0F020', '0415895');
INSERT INTO `tabla_inicial` VALUES (42163, '1200291575', '80594294', 'JENNY PAREDES SILVA', 'DAM21RFES03', '2000020606', 'ZAPATOS PR LOAFERS ESTELA SOKSO DANY-08A', 'ESTELA SOKSO', 'DANY-08A          ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS PR', 'ESTELA SOKSO', 'LOAFERS', '35', -1, '2021-06-15', 62.54, -62.54, -11.26, -73.80, 69.28, -69.28, -12.47, -81.75, 'Nota Credito', '91273873', '', 'ZNC1', '0FC20 0262633', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (42164, '1200291896', '01012125', 'MARIA SOCORRO PALACIOS LLEMPEN', 'DAM21DMES01', '2000020112', 'ZAPATOS PR MOCASIN ESTELA SOKSO GIU-080', 'ESTELA SOKSO', 'GIU-080           ', 'CAMEL', 'MUJER', 'BASICO', 'ZAPATOS PR', 'ESTELA SOKSO', 'MOCASIN', '36', -1, '2021-06-15', 73.98, -73.98, -13.32, -87.30, 81.99, -81.99, -14.76, -96.75, 'Nota Credito', '91273873', '', 'ZNC1', '0FC20 0262633', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (42165, '1200251724', '43969035', 'KARIN SHEYLA DEL AGUILA ROJAS', 'DAM21RFNS03', '2000020834', 'SANDALIAS PULSERA-T GRENDHA 2GDA93', 'GRENDHA', '2GDA93            ', 'NEGRO', 'MUJER', 'BASICO', 'SANDALIAS', 'LICENCIA', 'PULSERA-T', '38', -1, '2021-06-15', 46.50, -46.50, -8.37, -54.87, 49.70, -49.70, -8.95, -58.65, 'Nota Credito', '91273873', '', 'ZNC1', '0FC20 0262633', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (42166, '1200291575', '80594294', 'JENNY PAREDES SILVA', 'OFT21BR0005', '2000011648', 'ZAPATOS REYNA BEIRA RIO 4076150', 'BEIRA RIO', '4076150           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'REYNA', '37', -1, '2021-06-15', 53.59, -53.59, -9.65, -63.24, 57.31, -57.31, -10.32, -67.63, 'Nota Credito', '91273881', '', 'ZNC1', '0FC20 0262641', '', '0F020', '0413856');
INSERT INTO `tabla_inicial` VALUES (42167, '1200316234', '46995051', 'FIYORELY MABEL CABALLERO ABAD', 'CAB21OI0003', '2000020459', 'ZAPATILLAS LIFESTYLE FILA 11J617X', 'FILA', '11J617X           ', 'NEGRO/BLANCO', 'HOMBRE', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'LIFESTYLE', '39', -1, '2021-06-15', 73.30, -73.30, -13.19, -86.49, 78.51, -78.51, -14.13, -92.64, 'Nota Credito', '91273874', '', 'ZNC1', '0FC20 0262634', '', '0F020', '0412554');
INSERT INTO `tabla_inicial` VALUES (42168, '1200307468', '72512941', 'DIOMERLI SANTOS LABAN', 'DAM21OINS04', '2000019902', 'ZAPATILLAS PR URBANO NINA SIMIK Q5-075', 'NINA SIMIK', 'Q5-075            ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'NINA SIMIK', 'URBANO', '36', -1, '2021-06-15', 39.66, -39.66, -7.14, -46.80, 43.85, -43.85, -7.89, -51.74, 'Nota Credito', '91273877', '', 'ZNC1', '0FC20 0262637', '', '0F020', '0414614');
INSERT INTO `tabla_inicial` VALUES (42169, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'OFT21OF0008', '2000018512', 'SANDALIAS SLIDE NINA SIMIK SAN-172', 'NINA SIMIK', 'SAN-172           ', 'ACERO/BLANCO', 'MUJER', 'BASICO', 'SANDALIAS', 'NINA SIMIK', 'SLIDE', '37', -1, '2021-06-15', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91273877', '', 'ZNC1', '0FC20 0262637', '', '0F020', '0414614');
INSERT INTO `tabla_inicial` VALUES (42170, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'OFT21OF0008', '2000017433', 'ZAPATOS REYNA SOKSO SUN-242', 'SOKSO', 'SUN-242           ', 'ACERO', 'MUJER', 'BASICO', 'ZAPATOS', 'SOKSO', 'REYNA', '37', -1, '2021-06-15', 51.10, -51.10, -9.20, -60.30, 56.56, -56.56, -10.18, -66.74, 'Nota Credito', '91273877', '', 'ZNC1', '0FC20 0262637', '', '0F020', '0414614');
INSERT INTO `tabla_inicial` VALUES (42273, '1200295731', '41733632', 'JOBANI VASQUEZ DEL CASTILLO', 'DAM21OINS04', '2000019902', 'ZAPATILLAS PR URBANO NINA SIMIK Q5-075', 'NINA SIMIK', 'Q5-075            ', 'CELESTE', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'NINA SIMIK', 'URBANO', '36', -1, '2021-06-16', 39.66, -39.66, -7.14, -46.80, 43.85, -43.85, -7.89, -51.74, 'Nota Credito', '91275192', '', 'ZNC1', '0FC20 0263216', '', '0F020', '0416554');
INSERT INTO `tabla_inicial` VALUES (42274, '1200050585', '01013477', 'GLADIS AREVALO RODRIGUEZ', 'OFT21BR0005', '2000020396', 'SANDALIAS TIJERA GRENDHA 2GDA31', 'GRENDHA', '2GDA31            ', 'ROSA', 'MUJER', 'BASICO', 'SANDALIAS', 'LICENCIA', 'TIJERA', '36', -1, '2021-06-16', 29.16, -29.16, -5.25, -34.41, 31.36, -31.36, -5.65, -37.01, 'Nota Credito', '91275184', '', 'ZNC1', '0FC20 0263215', '', '0F020', '0415895');
INSERT INTO `tabla_inicial` VALUES (42275, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'DAM21OINS04', '2000019902', 'ZAPATILLAS PR URBANO NINA SIMIK Q5-075', 'NINA SIMIK', 'Q5-075            ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'NINA SIMIK', 'URBANO', '38', -1, '2021-06-16', 39.66, -39.66, -7.14, -46.80, 43.85, -43.85, -7.89, -51.74, 'Nota Credito', '91275195', '', 'ZNC1', '0FC20 0263218', '', '0F020', '0415895');
INSERT INTO `tabla_inicial` VALUES (42276, '1200251724', '43969035', 'KARIN SHEYLA DEL AGUILA ROJAS', 'CAB21DP0001', '2000020701', 'BOTAS OUTDOOR AMADEO ASTO GGW-015', 'AMADEO ASTO', 'GGW-015           ', 'AFRICANO', 'HOMBRE', 'BASICO', 'BOTAS', 'AMADEO ASTO', 'OUTDOOR', '42', -1, '2021-06-16', 159.41, -159.41, -28.69, -188.10, 177.33, -177.33, -31.92, -209.25, 'Nota Credito', '91275183', '', 'ZNC1', '0FC20 0263214', '', '0F020', '0417385');
INSERT INTO `tabla_inicial` VALUES (42285, '1200326589', '75335013', 'YHADIRA LISBETH CALDERON RITUAY', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'NEGRO', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -1, '2021-06-16', 31.27, -31.27, -5.63, -36.90, 34.64, -34.64, -6.24, -40.88, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (42286, '1200326589', '75335013', 'YHADIRA LISBETH CALDERON RITUAY', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'NEGRO', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -2, '2021-06-16', 31.28, -62.55, -11.26, -73.81, 34.64, -69.28, -12.47, -81.75, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (42287, '1200326589', '75335013', 'YHADIRA LISBETH CALDERON RITUAY', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'AZUL', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -2, '2021-06-16', 31.28, -62.55, -11.26, -73.81, 34.64, -69.28, -12.47, -81.75, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (42288, '1200325192', '71010913', 'MARCIA NATALIA MACHOA LINARES', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'AZUL', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -1, '2021-06-16', 31.27, -31.27, -5.63, -36.90, 34.64, -34.64, -6.24, -40.88, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (42289, '1200325192', '71010913', 'MARCIA NATALIA MACHOA LINARES', 'CAB21DP0001', '2000018594', 'ZAPATILLA LIFESTYLE URBAN CREEDS QTE-113', 'URBAN CREEDS', 'QTE-113           ', 'NEGRO', 'HOMBRE', 'BASICO', 'ZAPATILLAS PR', 'URBAN CREEDS', 'LIFESTYLE', '40', -1, '2021-06-16', 31.27, -31.27, -5.63, -36.90, 34.64, -34.64, -6.24, -40.88, 'Nota Credito', '91275182', '', 'ZNC1', '0FC20 0263213', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (42290, '1200288057', '33819205', 'EMILIA AGUIRRE MALQUI', 'DAM21OISK04', '2000019646', 'ZAPATILLAS PR URBANO SOKSO ADO-185', 'SOKSO', 'ADO-185           ', 'ACERO', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'SOKSO', 'URBANO', '37', -1, '2021-06-16', 51.10, -51.10, -9.20, -60.30, 56.56, -56.56, -10.18, -66.74, 'Nota Credito', '91275193', '', 'ZNC1', '0FC20 0263217', '', '0F020', '0416553');
INSERT INTO `tabla_inicial` VALUES (42298, '1200312356', '47554312', 'ERLINDA HERNANDEZ CAMPOS', 'KID21OI0003', '2000020753', 'SANDALIAS PULSERA JACK & JACKIE PAS-303', 'JACK & JACKIE', 'PAS-303           ', 'CELESTE', 'GIRLS', 'BASICO', 'SANDALIAS', 'JACK & JACKIE', 'PULSERA', '30', -1, '2021-06-01', 51.10, -51.10, -9.20, -60.30, 56.56, -56.56, -10.18, -66.74, 'Nota Credito', '91266227', '', 'ZNC1', '0FC20 0259512', '', '0F020', '0409326');
INSERT INTO `tabla_inicial` VALUES (42299, '1200251724', '43969035', 'KARIN SHEYLA DEL AGUILA ROJAS', 'CAB21OI0003', '2100005045', 'TOPS CAMISA ML URBAN CREEDS CDR-358', 'URBAN CREEDS', 'CDR-358           ', 'ROJO/BLANCO', 'HOMBRE', 'BASICO', 'TOPS', 'URBAN CREEDS', 'CAMISA ML', 'XL', -1, '2021-06-01', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91266246', '', 'ZNC1', '0FC20 0259517', '', '0F020', '0411836');
INSERT INTO `tabla_inicial` VALUES (42300, '1200296649', '46494210', 'MARIBEL NORIEGA BARRIOS', 'TMP21UD0017', '2000015709', 'ZAPATOS BALERINA MOLECA 5301309', 'MOLECA', '5301309           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'BALERINAS', '36', -1, '2021-06-01', 29.16, -29.16, -5.25, -34.41, 31.36, -31.36, -5.65, -37.01, 'Nota Credito', '91266234', '', 'ZNC1', '0FC20 0259513', '', '0F020', '0407747');
INSERT INTO `tabla_inicial` VALUES (42301, '1200288057', '33819205', 'EMILIA AGUIRRE MALQUI', 'DAM21DMSK01', '2000016946', 'ZAPATILLAS PR URBANO SOKSO ADO-149', 'SOKSO', 'ADO-149           ', 'AZUL', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'SOKSO', 'URBANO', '39', -1, '2021-06-01', 45.00, -45.00, -8.10, -53.10, 50.21, -50.21, -9.04, -59.25, 'Nota Credito', '91266248', '', 'ZNC1', '0FC20 0259519', '', '0F020', '0407880');
INSERT INTO `tabla_inicial` VALUES (42302, '1200291575', '80594294', 'JENNY PAREDES SILVA', 'OFT21BR0005', '2000011648', 'ZAPATOS REYNA BEIRA RIO 4076150', 'BEIRA RIO', '4076150           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'REYNA', '36', -1, '2021-06-01', 53.59, -53.59, -9.65, -63.24, 57.31, -57.31, -10.32, -67.63, 'Nota Credito', '91266249', '', 'ZNC1', '0FC20 0259520', '', '0F020', '0412553');
INSERT INTO `tabla_inicial` VALUES (42303, '1200315420', '45010411', 'MARIA HERNANDEZ CAMPOS', 'KID21OI0003', '2000021242', 'ZAPATILLAS CROCS MICKEY MOUSE 2MC493', 'MICKEY MOUSE', '2MC493            ', 'AZUL', 'BOYS', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'CROCS', '26', -1, '2021-06-01', 46.50, -46.50, -8.37, -54.87, 49.70, -49.70, -8.95, -58.65, 'Nota Credito', '91266226', '', 'ZNC1', '0FC20 0259511', '', '0F020', '0408601');
INSERT INTO `tabla_inicial` VALUES (42304, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'TMP21UD0019', '2000018374', 'ZAPATOS OXFORD VIALE RUT-5068', 'VIALE', 'RUT-5068          ', 'BRONCE', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'OXFORD', '36', -1, '2021-06-01', 162.36, -162.36, -29.23, -191.59, 174.72, -174.72, -31.45, -206.17, 'Nota Credito', '91266243', '', 'ZNC1', '0FC20 0259514', '', '0F020', '0411146');
INSERT INTO `tabla_inicial` VALUES (42305, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'KID21OI0003', '2100005027', 'BOTTOMS JOGGER JACK & JACKIE KJG-11', 'JACK & JACKIE', 'KJG-11            ', 'VERDE MILITAR', 'BOYS', 'BASICO', 'BOTTOMS', 'JACK & JACKIE', 'JOGGER', '14', -1, '2021-06-01', 45.00, -45.00, -8.10, -53.10, 50.21, -50.21, -9.04, -59.25, 'Nota Credito', '91266245', '', 'ZNC1', '0FC20 0259516', '', '0F020', '0411351');
INSERT INTO `tabla_inicial` VALUES (42306, '1200303106', '47939446', 'KELY ROXANA MUÃ±OZ CATALAN', 'DAM21OISK04', '2000019628', 'SANDALIAS PULSERA SOKSO MNG-015', 'SOKSO', 'MNG-015           ', 'NEGRO', 'MUJER', 'BASICO', 'SANDALIAS', 'SOKSO', 'PULSERA', '35', -1, '2021-06-01', 73.98, -73.98, -13.32, -87.30, 81.99, -81.99, -14.76, -96.75, 'Nota Credito', '91266247', '', 'ZNC1', '0FC20 0259518', '', '0F020', '0412553');
INSERT INTO `tabla_inicial` VALUES (42307, '1200303123', '47613248', 'CANDY TRUJILLO MARTINEZ', 'TMP21UD0016', '2000020589', 'SANDALIAS PULSERA ZAXY 2GZ969', 'ZAXY', '2GZ969            ', 'NEGRO', 'MUJER', 'BASICO', 'SANDALIAS', 'LICENCIA', 'PULSERA', '35', -1, '2021-06-01', 33.89, -33.89, -6.10, -39.99, 36.52, -36.52, -6.57, -43.09, 'Nota Credito', '91266225', '', 'ZNC1', '0FC20 0259510', '', '0F020', '0406626');
INSERT INTO `tabla_inicial` VALUES (42308, '1200291575', '80594294', 'JENNY PAREDES SILVA', 'DAM21RFES03', '2000020598', 'ZAPATILLAS PR URBANO ESTELA SOKSO AL-08', 'ESTELA SOKSO', 'AL-08             ', 'TURQUEZA', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'ESTELA SOKSO', 'LIFESTYLE', '37', -1, '2021-06-01', 79.33, -79.33, -14.28, -93.61, 88.35, -88.35, -15.90, -104.25, 'Nota Credito', '91266244', '', 'ZNC1', '0FC20 0259515', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (42309, '1200050585', '01013477', 'GLADIS AREVALO RODRIGUEZ', 'KID21KL0003', '2000017963', 'ZAPATILLAS CAÑA BAJA KLIN KL-438', 'KLIN', 'KL-438            ', 'NEGRO/BLANCO', 'INFANTE GIRL', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'BAJA', '26', -1, '2021-06-01', 53.59, -53.59, -9.65, -63.24, 57.31, -57.31, -10.32, -67.63, 'Nota Credito', '91266244', '', 'ZNC1', '0FC20 0259515', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (42447, '1200326941', '75957698', 'YANIRA CARRION USQUIANO', 'OFT21EF0003', '2000013465', 'ZAPATILLAS LIFESTYLE FILA 51U326X', 'FILA', '51U326X           ', 'MARINO/BLANCO', 'MUJER', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'LIFESTYLE', '38', -1, '2021-06-15', 119.79, -119.79, -21.56, -141.35, 129.11, -129.11, -23.24, -152.35, 'Nota Credito', '91273883', '', 'ZNC1', '0FC20 0262643', '', '0F020', '0415348');
INSERT INTO `tabla_inicial` VALUES (42448, '1200316234', '46995051', 'FIYORELY MABEL CABALLERO ABAD', 'DAM21OINS04', '2000014220', 'ZAPATILLAS RUNNING OLK DAY-547', 'OLIMPIKUS', 'DAY-547           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'RUNNING', '39', -1, '2021-06-15', 133.19, -133.19, -23.97, -157.16, 143.34, -143.34, -25.80, -169.14, 'Nota Credito', '91273872', '', 'ZNC1', '0FC20 0262632', '', '0F020', '0412554');
INSERT INTO `tabla_inicial` VALUES (42449, '1200050585', '01013477', 'GLADIS AREVALO RODRIGUEZ', 'DAM21OISK04', '2000019633', 'ZAPATOS MOCASIN SOKSO HAR-1813', 'SOKSO', 'HAR-1813          ', 'MOSTAZA', 'MUJER', 'BASICO', 'ZAPATOS', 'SOKSO', 'MOCASIN', '37', -1, '2021-06-15', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91273872', '', 'ZNC1', '0FC20 0262632', '', '0F020', '0412554');
INSERT INTO `tabla_inicial` VALUES (42450, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'OFT21VH0006', '2000018707', 'ZAPATOS PULSERA PIAZZA MULTI-259', 'PIAZZA', 'MULTI-259         ', 'BEIGE', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'PULSERA', '37', -1, '2021-06-15', 89.85, -89.85, -16.17, -106.02, 96.90, -96.90, -17.44, -114.34, 'Nota Credito', '91273882', '', 'ZNC1', '0FC20 0262642', '', '0F020', '0414614');
INSERT INTO `tabla_inicial` VALUES (42451, '1200329534', '70782724', 'ERIKA VANESSA CARDENAS TORRES', 'OFT21OF0008', '2000018474', 'SANDALIAS SLIDE SOKSO CAM-111', 'SOKSO', 'CAM-111           ', 'NEGRO', 'MUJER', 'BASICO', 'SANDALIAS', 'SOKSO', 'SLIDE', '37', -1, '2021-06-15', 45.00, -45.00, -8.10, -53.10, 50.21, -50.21, -9.04, -59.25, 'Nota Credito', '91273884', '', 'ZNC1', '0FC20 0262644', '', '0F020', '0415895');
INSERT INTO `tabla_inicial` VALUES (42452, '1200326941', '75957698', 'YANIRA CARRION USQUIANO', 'OFT21BR0005', '2000020377', 'ZAPATILLAS URBANO BEIRA RIO 41941316', 'BEIRA RIO', '41941316          ', 'BLANCO', 'MUJER', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'URBANO', '36', -1, '2021-06-15', 65.42, -65.42, -11.78, -77.20, 70.63, -70.63, -12.71, -83.34, 'Nota Credito', '91273875', '', 'ZNC1', '0FC20 0262635', '', '0F020', '0413855');
INSERT INTO `tabla_inicial` VALUES (42453, '1200269045', '72793324', 'MIRIAN LENITH RIOS ORTIZ', 'OFT21VH0006', '2000018939', 'ZAPATILLAS RUNNING ROUTT ZUN-015', 'ROUTT', 'ZUN-015           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATILLAS', 'ROUTT', 'RUNNING', '36', -1, '2021-06-15', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91273876', '', 'ZNC1', '0FC20 0262636', '', '0F020', '0414613');
INSERT INTO `tabla_inicial` VALUES (42454, '1200269045', '72793324', 'MIRIAN LENITH RIOS ORTIZ', 'OFT21EF0003', '2000014951', 'ZAPATILLAS RUNNING FILA 11J615X', 'FILA', '11J615X           ', 'NEGRO', 'HOMBRE', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'RUNNING', '38', -1, '2021-06-15', 59.90, -59.90, -10.78, -70.68, 64.30, -64.30, -11.57, -75.87, 'Nota Credito', '91273876', '', 'ZNC1', '0FC20 0262636', '', '0F020', '0414613');
INSERT INTO `tabla_inicial` VALUES (42455, '1200291896', '01012125', 'MARIA SOCORRO PALACIOS LLEMPEN', 'KID21OI0003', '2000021229', 'ZAPATILLAS BAJA PRINCESAS 259-93561', 'PRINCESAS', '259-93561         ', 'CELESTE', 'GIRLS', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'BAJA', '29', -1, '2021-06-15', 33.11, -33.11, -5.96, -39.07, 35.30, -35.30, -6.35, -41.65, 'Nota Credito', '91273876', '', 'ZNC1', '0FC20 0262636', '', '0F020', '0414613');
INSERT INTO `tabla_inicial` VALUES (42462, '1200284793', '07639859', 'MARIA YOLANDA RAMIREZ DIAZ', 'TMP21UD0020', '2100004395', 'TOPS POLO MC FILA LS180387', 'FILA', 'LS180387          ', 'BLANCO', 'HOMBRE', 'BASICO', 'TOPS', 'LICENCIA', 'POLO MC', 'XL', -1, '2021-06-15', 29.16, -29.16, -5.25, -34.41, 31.36, -31.36, -5.65, -37.01, 'Nota Credito', '91273879', '', 'ZNC1', '0FC20 0262639', '', '0F020', '0414532');
INSERT INTO `tabla_inicial` VALUES (42481, '1200251724', '43969035', 'KARIN SHEYLA DEL AGUILA ROJAS', 'DAM21OISK04', '2000019633', 'ZAPATOS MOCASIN SOKSO HAR-1813', 'SOKSO', 'HAR-1813          ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS', 'SOKSO', 'MOCASIN', '36', -1, '2021-06-15', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91273880', '', 'ZNC1', '0FC20 0262640', '', '0F020', '0413244');
INSERT INTO `tabla_inicial` VALUES (42482, '1200307098', '41228621', 'WILMER PAZ ROMERO', 'CAB21OI0003', '2000021195', 'SANDALIAS PULSERA RIDER 2RDA77', 'RIDER', '2RDA77            ', 'NEGRO', 'HOMBRE', 'BASICO', 'SANDALIAS', 'LICENCIA', 'PULSERA', '41', -1, '2021-06-15', 52.81, -52.81, -9.51, -62.32, 56.91, -56.91, -10.24, -67.15, 'Nota Credito', '91273878', '', 'ZNC1', '0FC20 0262638', '', '0F020', '0415895');
INSERT INTO `tabla_inicial` VALUES (42483, '1200291575', '80594294', 'JENNY PAREDES SILVA', 'DAM21RFES03', '2000020606', 'ZAPATOS PR LOAFERS ESTELA SOKSO DANY-08A', 'ESTELA SOKSO', 'DANY-08A          ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS PR', 'ESTELA SOKSO', 'LOAFERS', '35', -1, '2021-06-15', 62.54, -62.54, -11.26, -73.80, 69.28, -69.28, -12.47, -81.75, 'Nota Credito', '91273873', '', 'ZNC1', '0FC20 0262633', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (42484, '1200291896', '01012125', 'MARIA SOCORRO PALACIOS LLEMPEN', 'DAM21DMES01', '2000020112', 'ZAPATOS PR MOCASIN ESTELA SOKSO GIU-080', 'ESTELA SOKSO', 'GIU-080           ', 'CAMEL', 'MUJER', 'BASICO', 'ZAPATOS PR', 'ESTELA SOKSO', 'MOCASIN', '36', -1, '2021-06-15', 73.98, -73.98, -13.32, -87.30, 81.99, -81.99, -14.76, -96.75, 'Nota Credito', '91273873', '', 'ZNC1', '0FC20 0262633', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (42485, '1200251724', '43969035', 'KARIN SHEYLA DEL AGUILA ROJAS', 'DAM21RFNS03', '2000020834', 'SANDALIAS PULSERA-T GRENDHA 2GDA93', 'GRENDHA', '2GDA93            ', 'NEGRO', 'MUJER', 'BASICO', 'SANDALIAS', 'LICENCIA', 'PULSERA-T', '38', -1, '2021-06-15', 46.50, -46.50, -8.37, -54.87, 49.70, -49.70, -8.95, -58.65, 'Nota Credito', '91273873', '', 'ZNC1', '0FC20 0262633', '', '0F020', '0411281');
INSERT INTO `tabla_inicial` VALUES (42486, '1200291575', '80594294', 'JENNY PAREDES SILVA', 'OFT21BR0005', '2000011648', 'ZAPATOS REYNA BEIRA RIO 4076150', 'BEIRA RIO', '4076150           ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATOS', 'LICENCIA', 'REYNA', '37', -1, '2021-06-15', 53.59, -53.59, -9.65, -63.24, 57.31, -57.31, -10.32, -67.63, 'Nota Credito', '91273881', '', 'ZNC1', '0FC20 0262641', '', '0F020', '0413856');
INSERT INTO `tabla_inicial` VALUES (42487, '1200316234', '46995051', 'FIYORELY MABEL CABALLERO ABAD', 'CAB21OI0003', '2000020459', 'ZAPATILLAS LIFESTYLE FILA 11J617X', 'FILA', '11J617X           ', 'NEGRO/BLANCO', 'HOMBRE', 'BASICO', 'ZAPATILLAS', 'LICENCIA', 'LIFESTYLE', '39', -1, '2021-06-15', 73.30, -73.30, -13.19, -86.49, 78.51, -78.51, -14.13, -92.64, 'Nota Credito', '91273874', '', 'ZNC1', '0FC20 0262634', '', '0F020', '0412554');
INSERT INTO `tabla_inicial` VALUES (42488, '1200307468', '72512941', 'DIOMERLI SANTOS LABAN', 'DAM21OINS04', '2000019902', 'ZAPATILLAS PR URBANO NINA SIMIK Q5-075', 'NINA SIMIK', 'Q5-075            ', 'NEGRO', 'MUJER', 'BASICO', 'ZAPATILLAS PR', 'NINA SIMIK', 'URBANO', '36', -1, '2021-06-15', 39.66, -39.66, -7.14, -46.80, 43.85, -43.85, -7.89, -51.74, 'Nota Credito', '91273877', '', 'ZNC1', '0FC20 0262637', '', '0F020', '0414614');
INSERT INTO `tabla_inicial` VALUES (42489, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'OFT21OF0008', '2000018512', 'SANDALIAS SLIDE NINA SIMIK SAN-172', 'NINA SIMIK', 'SAN-172           ', 'ACERO/BLANCO', 'MUJER', 'BASICO', 'SANDALIAS', 'NINA SIMIK', 'SLIDE', '37', -1, '2021-06-15', 56.44, -56.44, -10.16, -66.60, 62.92, -62.92, -11.33, -74.25, 'Nota Credito', '91273877', '', 'ZNC1', '0FC20 0262637', '', '0F020', '0414614');
INSERT INTO `tabla_inicial` VALUES (42490, '1200295504', '75516633', 'NERSY HOYOS CARRASCO', 'OFT21OF0008', '2000017433', 'ZAPATOS REYNA SOKSO SUN-242', 'SOKSO', 'SUN-242           ', 'ACERO', 'MUJER', 'BASICO', 'ZAPATOS', 'SOKSO', 'REYNA', '37', -1, '2021-06-15', 51.10, -51.10, -9.20, -60.30, 56.56, -56.56, -10.18, -66.74, 'Nota Credito', '91273877', '', 'ZNC1', '0FC20 0262637', '', '0F020', '0414614');

-- ----------------------------
-- Table structure for tipo_cliente
-- ----------------------------
DROP TABLE IF EXISTS `tipo_cliente`;
CREATE TABLE `tipo_cliente`  (
  `tipo_cliente_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_cliente_descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tipo_cliente_estado` int(255) NULL DEFAULT NULL,
  `tipo_cliente_ruc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`tipo_cliente_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tipo_cliente
-- ----------------------------
INSERT INTO `tipo_cliente` VALUES (1, 'DNI', 1, '8');

-- ----------------------------
-- Table structure for tipo_documento
-- ----------------------------
DROP TABLE IF EXISTS `tipo_documento`;
CREATE TABLE `tipo_documento`  (
  `tipodoc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipodoc_descripcion` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipodoc_abreviacion` varchar(7) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipodoc_estado` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `tipodoc_codigo` char(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`tipodoc_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tipo_documento
-- ----------------------------
INSERT INTO `tipo_documento` VALUES (1, 'FACTURA ELECTRONICA', 'FACTURA', '1', '01');
INSERT INTO `tipo_documento` VALUES (2, 'BOLETA ELECTRONICA', 'BOLETA', '1', '03');
INSERT INTO `tipo_documento` VALUES (3, 'NOTA DE CREDITO ', 'N. CRED', '1', '07');
INSERT INTO `tipo_documento` VALUES (4, 'NOTA DE DEBITO ', 'N. DEB', '1', '08');
INSERT INTO `tipo_documento` VALUES (5, 'TICKET DE MAQUINA REGISTRADORA', NULL, '1', '12');
INSERT INTO `tipo_documento` VALUES (6, 'SIN DOCUMENTO', 'SIN DOC', '1', '00');
INSERT INTO `tipo_documento` VALUES (7, 'GUIA DE REMISIÓN', 'GUIA', '1', '09');

-- ----------------------------
-- Table structure for venta
-- ----------------------------
DROP TABLE IF EXISTS `venta`;
CREATE TABLE `venta`  (
  `venta_idventas` int(50) NOT NULL AUTO_INCREMENT,
  `venta_idmoneda` int(11) NULL DEFAULT NULL,
  `ventas_idtipodocumento` int(11) NULL DEFAULT 0,
  `venta_tipopago` int(11) NULL DEFAULT NULL,
  `venta_formapago` int(11) NULL DEFAULT NULL,
  `venta_tipoventa` int(11) NULL DEFAULT NULL,
  `venta_codigocliente` int(11) NULL DEFAULT 0,
  `venta_fecha` datetime(0) NULL DEFAULT NULL,
  `venta_num_serie` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_num_documento` int(11) NULL DEFAULT NULL,
  `venta_monto` decimal(11, 2) NULL DEFAULT NULL,
  `venta_observaciones` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `venta_estado` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `venta_fechapedido` int(25) NULL DEFAULT NULL,
  `venta_pedidofecha` datetime(0) NULL DEFAULT NULL,
  `ventas_vuelto` decimal(11, 2) NULL DEFAULT NULL,
  `venta_credito_estado` int(11) NULL DEFAULT NULL,
  `venta_credito_cuotas` int(11) NULL DEFAULT NULL,
  `venta_credito_usuario` int(11) NULL DEFAULT NULL,
  `venta_fecha_pago` datetime(0) NULL DEFAULT NULL,
  `venta_monto_entregado` float(44, 2) NULL DEFAULT NULL,
  `venta_igv_estado` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_igv_monto` float(44, 2) NULL DEFAULT NULL,
  `venta_monto_sinigv` float(44, 2) NULL DEFAULT NULL,
  `venta_fecha_eliminacion` datetime(0) NULL DEFAULT NULL,
  `venta_empleado_eliminacion` int(11) NULL DEFAULT NULL,
  `venta_eliminacion_descripcion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_tidocelimi` int(11) NULL DEFAULT NULL,
  `venta_serie_eliminado` char(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_correlativo_eliminado` int(11) NULL DEFAULT NULL,
  `venta_idpersonal_eliminado` int(11) NULL DEFAULT NULL,
  `venta_pdf_facturacion` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `venta_xml_facturacion` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `venta_cdr_facturacion` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `venta_ticket_facturacion` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `venta_comprobante_aceptado` int(11) NULL DEFAULT 0,
  `venta_estado_consumo` int(11) NOT NULL DEFAULT 0,
  `venta_estado_agrupacion` int(11) NOT NULL DEFAULT 0,
  `ventaid_tipo_venta` int(11) NULL DEFAULT NULL,
  `venta_nombre_descripcion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_documento_descripcion` varchar(255) CHARACTER SET latin2 COLLATE latin2_general_ci NULL DEFAULT NULL,
  `venta_direccion_descripcion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_ticket_external_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_resumen_external_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_resumen_ticket` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_resumen_fecha` datetime(0) NULL DEFAULT NULL,
  `venta_resumen_xml` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_resumen_cdr` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_resumen_cdr_fecha` datetime(0) NULL DEFAULT NULL,
  `venta_qr` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `venta_hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_number_to_letter` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_motivo_eliminacion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_id_cajero` int(255) NULL DEFAULT NULL,
  `venta_estado_facturacion` int(15) NULL DEFAULT 0,
  `venta_correo_cliente` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `venta_tipo_comprobante_factura` int(255) NULL DEFAULT 1,
  PRIMARY KEY (`venta_idventas`) USING BTREE,
  INDEX `fk_venta_moneda`(`venta_idmoneda`) USING BTREE,
  INDEX `fk_venta_tipopago`(`venta_tipopago`) USING BTREE,
  INDEX `fk_venta_formapago`(`venta_formapago`) USING BTREE,
  INDEX `fk_venta_cliente`(`venta_codigocliente`) USING BTREE,
  INDEX `fk_venta_tipdocumento`(`ventas_idtipodocumento`) USING BTREE,
  INDEX `venta_credito_usuario`(`venta_credito_usuario`) USING BTREE,
  INDEX `ventaid_tipo_venta`(`ventaid_tipo_venta`) USING BTREE,
  CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`venta_formapago`) REFERENCES `formapago` (`for_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `venta_ibfk_2` FOREIGN KEY (`venta_codigocliente`) REFERENCES `cliente` (`cliente_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 14212 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Procedure structure for cargar_boleta
-- ----------------------------
DROP PROCEDURE IF EXISTS `cargar_boleta`;
delimiter ;;
CREATE PROCEDURE `cargar_boleta`()
BEGIN





DECLARE num_comprobante,venta_id int;
DECLARE comprobante  varchar(4);
DECLARE done INT DEFAULT FALSE;
DECLARE dni1,dni,nombre,fecha,correo,tipo_comprobante,direccion varchar(255);
DECLARE total,igv,subtotal DOUBLE(10,2);	

DECLARE cargarventa  CURSOR FOR 
SELECT if(cliente.cliente_tipo_comprobabte is null,
				 tabla_inicial_dni,
				 if(cliente.cliente_tipo_comprobabte=1,tabla_inicial_dni,cliente.cliente_ruc)
				) as 'dni',
				
		if(cliente.cliente_tipo_comprobabte is null,	
			tabla_inicial_nombre,
			if(
			cliente.cliente_tipo_comprobabte=1,tabla_inicial_nombre,cliente.cliente_razon_social
			)
			
		)as 'nombre',
		
			 tabla_inicial_fecha as 'fecha_venta',
			 SUM(round(tabla_inicial_precio_promocion+(0.18*tabla_inicial_precio_promocion),0)*tabla_inicial_cantidad) as 'total',
			 SUM(round(tabla_inicial_precio_promocion+(0.18*tabla_inicial_precio_promocion),0)*tabla_inicial_cantidad*0) as 'igv',
			 SUM(round(tabla_inicial_precio_promocion+(0.18*tabla_inicial_precio_promocion),0)*tabla_inicial_cantidad*1) as 'subtotal',
			 if(cliente.cliente_correo is null ,'',cliente.cliente_correo) as correo,
			if(cliente.cliente_tipo_comprobabte is null,'1',if(cliente.cliente_tipo_comprobabte=1
			,'2','1')) as 'tipo_comprobante',
			
			if(cliente.cliente_tipo_comprobabte is null,	
						'-',
						if(
						cliente.cliente_tipo_comprobabte=1,cliente.cliente_direccion,cliente.cliente_direccion_empresa
						)
			
					)as 'direccion',
					tabla_inicial_dni as 'dni1'
			 
			 from tabla_inicial
			 left join cliente on cliente.cliente_documento=tabla_inicial.tabla_inicial_dni
			 where tabla_inicial_cantidad>0
			 GROUP BY tabla_inicial_fecha,tabla_inicial_dni
			 ORDER BY tabla_inicial_dni; 
			
			
	
			
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
#cargar documento 

CALL verificar_informacion();
/*insert into select from*/
#SELECT comprobante;
	#Routine body goes here...		
  OPEN cargarventa;
	  read_loop: LOOP
    FETCH cargarventa INTO dni,nombre,fecha,total,igv,subtotal,correo,tipo_comprobante,direccion,dni1;
		  IF done THEN
       LEAVE read_loop;
      END IF;
			
			 set comprobante=(SELECT documento.doc_serie from documento where documento.id_tipodocumento=tipo_comprobante);
set  num_comprobante= (SELECT documento.doc_correlativo from documento where documento.id_tipodocumento=tipo_comprobante);
	insert into venta(
			venta_idmoneda,
			ventas_idtipodocumento,
			venta_tipopago,
			venta_formapago,
			venta_tipoventa,
			venta_fecha,
			venta_num_serie,
			venta_num_documento,
			venta_codigocliente,
			venta_monto,
			ventas_vuelto,
			venta_monto_entregado,
			venta_nombre_descripcion,
			venta_documento_descripcion,
			venta_direccion_descripcion,
			ventaid_tipo_venta,
			venta_fecha_pago,
			venta_igv_estado,
			venta_igv_monto,
			venta_monto_sinigv,
			venta_id_cajero,
			venta_correo_cliente,
			venta_tipo_comprobante_factura
			) VALUES(
			1,
			tipo_comprobante,
			1,
			1,
			1,
			fecha,
			comprobante,
			num_comprobante,
			1,
			total,
			0,
			total,
			nombre,
			dni,
			direccion,
			1,
			fecha,
			0,
			0,
			total,
			1,
			correo,
			1
			);
			 set venta_id=(SELECT MAX(venta.venta_idventas) from venta);
			 insert into 
			 detalle_venta(descripcion,
			 cantidad,
			 precio,
			 id_venta,
			 detalle_venta_marca,
			 detalle_venta_modelo,
			 detalle_venta_color,
			 detalle_venta_familia,
			 detalle_venta_categoria,
			 detalle_venta_tipo_marca,
			 detalle_venta_submarca,
			 detalle_venta_talla,
			 detalle_venta_documento,
			 detalle_codigo_producto,
			 detalle_fecha_venta,
			 		/*agregar*/
				detalle_cod_producto,
			detalle_tipo_documento,
			detalle_documento_identidad
			
			 )
			 
			 
			select tabla_inicial_descripcion_producto,
						 tabla_inicial_cantidad,
						 ROUND(tabla_inicial_precio_promocion+(0.18*tabla_inicial_precio_promocion)) as 'igv',
						 venta_id	,
						 tabla_inicial_marca,
						 table_inicial_modelo,
						 tabla_inicial_color,
						 tabla_inicial_familia,
						 tabla_inicial_categoria,
						 tabla_inicial_tipo_marca,
						 tabla_inicial_sub_categoria,
						 tabla_inicial_talla,
						 tabla_inicial_t_numero,
						 tabla_inicial_codigo_producto,
						 tabla_inicial_fecha,
						 tabla_inicial_codigo_producto,
						 tabla_inicial_t_doc,
						 tabla_inicial_dni
						 
			from tabla_inicial
			where tabla_inicial_cantidad>0 and
			tabla_inicial_fecha=fecha and tabla_inicial_dni=dni1
			ORDER BY tabla_inicial_id;
			
			set num_comprobante=num_comprobante+1;
			
			update documento set documento.doc_correlativo=num_comprobante where documento.id_tipodocumento=tipo_comprobante;
			
			
		/*	SELECT c;*/
		
	 END LOOP read_loop;

	delete from tabla_inicial where tabla_inicial_cantidad>0 ;


END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for verificar_informacion
-- ----------------------------
DROP PROCEDURE IF EXISTS `verificar_informacion`;
delimiter ;;
CREATE PROCEDURE `verificar_informacion`()
BEGIN
DECLARE dni,cod_producto,cod_venta,color varchar(255);
DECLARE fecha date;
DECLARE id,id1 int(11);
DECLARE contador int(11) DEFAULT 0;
DECLARE contador_no int(11) DEFAULT 0;
DECLARE faltante VARCHAR(230) DEFAULT '';
	DECLARE done INT DEFAULT FALSE;
  DECLARE  tabla CURSOR FOR SELECT tabla_inicial_id,tabla_inicial_dni,tabla_inicial_fecha,tabla_inicial_codigo_producto
	,tabla_inicial_t_numero,tabla_inicial_color
	from tabla_inicial where tabla_inicial_cantidad>0;


  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	  OPEN tabla;
	  read_loop: LOOP
    FETCH tabla INTO id,dni,fecha,cod_producto,cod_venta,color;
		  IF done THEN
       LEAVE read_loop;
      END IF;
			
			


  IF ( SELECT EXISTS (SELECT 1 FROM detalle_venta 
				WHERE detalle_fecha_venta=fecha 
				and detalle_documento_identidad=dni
				and detalle_codigo_producto=cod_producto
				and detalle_venta_documento=cod_venta
				and detalle_venta_color=color
				
				) ) THEN
				delete from tabla_inicial where tabla_inicial_id=id;
				SET contador=contador+1;
				
		ELSE 
			 SET contador_no=contador_no+1;
			 SET faltante=CONCAT(faltante,",",dni,'-',id);
				
       /* SELECT 'EXISTS';*/
			/*delete from tabla_inicial where tabla_inicial_id=id;*/
	  
	  /* SELECT 'no existen';*/
	
    END IF;  
	 
  END LOOP read_loop;
	
	SELECT contador,contador_no,faltante;


END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
